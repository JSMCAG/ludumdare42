{
    "id": "ceb7e267-8f31-4065-bd86-d31323ceb54f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boss_appear",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f5569b1c-b9cb-42cf-a52a-9808ce8af60e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ceb7e267-8f31-4065-bd86-d31323ceb54f",
            "compositeImage": {
                "id": "428de2b1-ed5a-46df-b206-a2f02bba53c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5569b1c-b9cb-42cf-a52a-9808ce8af60e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b56778e-5dbf-424c-902f-1dbddc9d3856",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5569b1c-b9cb-42cf-a52a-9808ce8af60e",
                    "LayerId": "7ca812a6-909e-49e5-b421-3e4e4f8332e3"
                }
            ]
        },
        {
            "id": "e1c3fedf-a5e6-4221-ad97-728e8f768829",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ceb7e267-8f31-4065-bd86-d31323ceb54f",
            "compositeImage": {
                "id": "540c2520-f425-449d-bedc-a6d788818c5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1c3fedf-a5e6-4221-ad97-728e8f768829",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20ba18dc-4f9e-4709-93b5-4c03e14dfcd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1c3fedf-a5e6-4221-ad97-728e8f768829",
                    "LayerId": "7ca812a6-909e-49e5-b421-3e4e4f8332e3"
                }
            ]
        },
        {
            "id": "7c108cc2-e6b7-4749-a996-d94a588142a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ceb7e267-8f31-4065-bd86-d31323ceb54f",
            "compositeImage": {
                "id": "5d2047da-e400-4394-9dd5-d5f975a48af6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c108cc2-e6b7-4749-a996-d94a588142a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b2df1ba-760e-41ed-83b5-d21e8a54562b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c108cc2-e6b7-4749-a996-d94a588142a8",
                    "LayerId": "7ca812a6-909e-49e5-b421-3e4e4f8332e3"
                }
            ]
        },
        {
            "id": "87f12b49-4957-4c13-b71a-3062728fefff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ceb7e267-8f31-4065-bd86-d31323ceb54f",
            "compositeImage": {
                "id": "0a208dbd-9b1c-45e5-b06b-42e769f1ce2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87f12b49-4957-4c13-b71a-3062728fefff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d87f8f6d-aa24-4f50-beef-ff48df28463a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87f12b49-4957-4c13-b71a-3062728fefff",
                    "LayerId": "7ca812a6-909e-49e5-b421-3e4e4f8332e3"
                }
            ]
        },
        {
            "id": "c9fa61d1-599b-4f9e-b51a-10e284bc81f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ceb7e267-8f31-4065-bd86-d31323ceb54f",
            "compositeImage": {
                "id": "51d07dfd-06f7-4c13-83e9-4910bb578bc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9fa61d1-599b-4f9e-b51a-10e284bc81f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f43a8fc9-3ed5-4d8c-a42e-0d2df496ef63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9fa61d1-599b-4f9e-b51a-10e284bc81f6",
                    "LayerId": "7ca812a6-909e-49e5-b421-3e4e4f8332e3"
                }
            ]
        },
        {
            "id": "0e6e6953-65f8-42ec-adbf-b1c5eeb082aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ceb7e267-8f31-4065-bd86-d31323ceb54f",
            "compositeImage": {
                "id": "13a35f7f-88fd-47f2-a48d-6c62a82ed779",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e6e6953-65f8-42ec-adbf-b1c5eeb082aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40794c3c-fd47-465a-b999-f761e7d573e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e6e6953-65f8-42ec-adbf-b1c5eeb082aa",
                    "LayerId": "7ca812a6-909e-49e5-b421-3e4e4f8332e3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7ca812a6-909e-49e5-b421-3e4e4f8332e3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ceb7e267-8f31-4065-bd86-d31323ceb54f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 14
}