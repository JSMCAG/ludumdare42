{
    "id": "c337be44-da8f-4658-abcb-f3d8180cfea6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_attacking_d",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6cd15133-df1a-43b0-9d58-896a8bb775a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c337be44-da8f-4658-abcb-f3d8180cfea6",
            "compositeImage": {
                "id": "a6789776-911e-4787-a48c-1dda0de9aea1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cd15133-df1a-43b0-9d58-896a8bb775a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "baba46f1-3896-459a-b732-6356399b7568",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cd15133-df1a-43b0-9d58-896a8bb775a5",
                    "LayerId": "e64e4c85-52df-469d-afbf-7923f22b2150"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e64e4c85-52df-469d-afbf-7923f22b2150",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c337be44-da8f-4658-abcb-f3d8180cfea6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 14
}