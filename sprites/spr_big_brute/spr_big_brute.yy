{
    "id": "ab34ed29-5724-4cb1-99ef-18715a420f4a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_big_brute",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 19,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "93f1f068-825b-495b-b590-2b2f922bedaa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab34ed29-5724-4cb1-99ef-18715a420f4a",
            "compositeImage": {
                "id": "5eea857b-3cf2-4fce-9ca3-4645b24fa71d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93f1f068-825b-495b-b590-2b2f922bedaa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb388356-ef37-41d7-9348-70581fe01509",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93f1f068-825b-495b-b590-2b2f922bedaa",
                    "LayerId": "530ef380-0c56-4ce8-b17b-cb3bc147ce07"
                }
            ]
        },
        {
            "id": "3d71540d-95cd-4bc3-a842-2b8e072ff776",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab34ed29-5724-4cb1-99ef-18715a420f4a",
            "compositeImage": {
                "id": "a93fc73b-1bcd-4332-8bc5-e200ea0b2ac3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d71540d-95cd-4bc3-a842-2b8e072ff776",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45022c22-66fd-4756-8411-55f1db2c0674",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d71540d-95cd-4bc3-a842-2b8e072ff776",
                    "LayerId": "530ef380-0c56-4ce8-b17b-cb3bc147ce07"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "530ef380-0c56-4ce8-b17b-cb3bc147ce07",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ab34ed29-5724-4cb1-99ef-18715a420f4a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 31
}