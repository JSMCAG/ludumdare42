{
    "id": "1d5e6471-1b9e-4a0d-8a39-46a14ee8bee5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lightning",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3d1a1837-d741-4deb-9ec4-3c91fcc5e95a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d5e6471-1b9e-4a0d-8a39-46a14ee8bee5",
            "compositeImage": {
                "id": "ec30d9e6-1686-42ca-8271-8c53b9d6fc39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d1a1837-d741-4deb-9ec4-3c91fcc5e95a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35fc5c8e-24fe-40f5-9b94-e2000437ff66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d1a1837-d741-4deb-9ec4-3c91fcc5e95a",
                    "LayerId": "bc5f0fe9-529b-4aa9-951d-49bde0478a4c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "bc5f0fe9-529b-4aa9-951d-49bde0478a4c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1d5e6471-1b9e-4a0d-8a39-46a14ee8bee5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 7
}