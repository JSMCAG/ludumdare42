{
    "id": "2056ddec-0c93-45ef-9dad-48dc611b1483",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_test",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "590eb9e2-e15a-43cb-aeb3-bd1ec49f1df5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2056ddec-0c93-45ef-9dad-48dc611b1483",
            "compositeImage": {
                "id": "a558c166-e697-4e6c-9041-2f95a6cc95f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "590eb9e2-e15a-43cb-aeb3-bd1ec49f1df5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfc6643d-6332-4066-ae5a-bfa009e93af3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "590eb9e2-e15a-43cb-aeb3-bd1ec49f1df5",
                    "LayerId": "b170f88d-f9a1-4834-8358-bcd75ec26ce1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "b170f88d-f9a1-4834-8358-bcd75ec26ce1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2056ddec-0c93-45ef-9dad-48dc611b1483",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}