{
    "id": "75474d3a-83b9-467e-b2dd-7a34261a6ae2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_scroll_pedestal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4acdca97-a162-4d80-bf3a-1a6924a809ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75474d3a-83b9-467e-b2dd-7a34261a6ae2",
            "compositeImage": {
                "id": "298d37b8-eee9-4bb8-9bd1-af0a32ee6798",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4acdca97-a162-4d80-bf3a-1a6924a809ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ebd5688-b291-4c3a-8a0b-eab8001bdd35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4acdca97-a162-4d80-bf3a-1a6924a809ba",
                    "LayerId": "0e235cef-8a0e-41ca-8c7f-83fa519a2604"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "0e235cef-8a0e-41ca-8c7f-83fa519a2604",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75474d3a-83b9-467e-b2dd-7a34261a6ae2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 7
}