{
    "id": "f5fc441d-13ec-418a-b99b-ae8f3ba9d6cf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shadow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "23eb44e2-bc0e-48d1-9c0d-97354b94953a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5fc441d-13ec-418a-b99b-ae8f3ba9d6cf",
            "compositeImage": {
                "id": "0e0f8fda-7836-48e6-934a-abf3e9241427",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23eb44e2-bc0e-48d1-9c0d-97354b94953a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76247968-81f0-49a4-b542-602be5cc614d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23eb44e2-bc0e-48d1-9c0d-97354b94953a",
                    "LayerId": "0531c306-d123-46ea-8e1d-5417fccdc3ef"
                }
            ]
        },
        {
            "id": "f1552d66-98be-493b-b6d7-d44b7a6bd011",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5fc441d-13ec-418a-b99b-ae8f3ba9d6cf",
            "compositeImage": {
                "id": "688b4ab9-ddc6-4084-b50d-e5ba888d9597",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1552d66-98be-493b-b6d7-d44b7a6bd011",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "269ed5b3-eaf3-41c8-8c40-22af482e6225",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1552d66-98be-493b-b6d7-d44b7a6bd011",
                    "LayerId": "0531c306-d123-46ea-8e1d-5417fccdc3ef"
                }
            ]
        },
        {
            "id": "915aacb3-3dee-40c0-aabb-3de5cfd90b87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5fc441d-13ec-418a-b99b-ae8f3ba9d6cf",
            "compositeImage": {
                "id": "caff03f4-fa9b-485e-afb9-148ffd5c310f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "915aacb3-3dee-40c0-aabb-3de5cfd90b87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09a1c8e6-b0dc-4c11-85fa-ec6a909c99eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "915aacb3-3dee-40c0-aabb-3de5cfd90b87",
                    "LayerId": "0531c306-d123-46ea-8e1d-5417fccdc3ef"
                }
            ]
        },
        {
            "id": "e04e8690-7bdd-4175-bcb5-624a84119432",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5fc441d-13ec-418a-b99b-ae8f3ba9d6cf",
            "compositeImage": {
                "id": "e8f87429-2c67-4de3-b25c-4b7f4d74f85e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e04e8690-7bdd-4175-bcb5-624a84119432",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e653cea-bf59-4fe5-9f9c-3dd883ef45ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e04e8690-7bdd-4175-bcb5-624a84119432",
                    "LayerId": "0531c306-d123-46ea-8e1d-5417fccdc3ef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "0531c306-d123-46ea-8e1d-5417fccdc3ef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f5fc441d-13ec-418a-b99b-ae8f3ba9d6cf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 6
}