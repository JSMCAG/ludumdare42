{
    "id": "ad0674c3-9e37-4f66-8ece-e5371c023962",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fake_shadow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ded7e1ed-464a-4e85-bd07-9602de5f4195",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad0674c3-9e37-4f66-8ece-e5371c023962",
            "compositeImage": {
                "id": "141b65fa-9dc1-49fc-bb40-cecffaeeee55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ded7e1ed-464a-4e85-bd07-9602de5f4195",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9572dde-8ff8-4dad-b9ea-5de82e9de3e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ded7e1ed-464a-4e85-bd07-9602de5f4195",
                    "LayerId": "fd7b63e4-e769-4680-9a97-9248a61f36ac"
                }
            ]
        },
        {
            "id": "756938c6-2fff-4ede-b162-3a92a32fa155",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad0674c3-9e37-4f66-8ece-e5371c023962",
            "compositeImage": {
                "id": "60bba359-df76-4c3a-99d3-9f39cf214de7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "756938c6-2fff-4ede-b162-3a92a32fa155",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cdbdb8a-b1b7-48d6-8fd8-6f2d1712ab36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "756938c6-2fff-4ede-b162-3a92a32fa155",
                    "LayerId": "fd7b63e4-e769-4680-9a97-9248a61f36ac"
                }
            ]
        },
        {
            "id": "00d64399-22b6-46ce-a401-f1870796fd82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad0674c3-9e37-4f66-8ece-e5371c023962",
            "compositeImage": {
                "id": "bff986c1-cbf3-4a9b-93a0-5244d4f63eba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00d64399-22b6-46ce-a401-f1870796fd82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d42bf7ad-901e-4fa0-b2e6-10441d99adb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00d64399-22b6-46ce-a401-f1870796fd82",
                    "LayerId": "fd7b63e4-e769-4680-9a97-9248a61f36ac"
                }
            ]
        },
        {
            "id": "5efa03aa-bb1e-41ee-98e4-e634bfd474d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad0674c3-9e37-4f66-8ece-e5371c023962",
            "compositeImage": {
                "id": "94ed1b9d-2a5d-45c3-ac7a-2740ff7b262a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5efa03aa-bb1e-41ee-98e4-e634bfd474d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56287e22-1865-40e7-bd4c-79fb1ab27c5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5efa03aa-bb1e-41ee-98e4-e634bfd474d2",
                    "LayerId": "fd7b63e4-e769-4680-9a97-9248a61f36ac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "fd7b63e4-e769-4680-9a97-9248a61f36ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ad0674c3-9e37-4f66-8ece-e5371c023962",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 6
}