{
    "id": "cc73719e-1f5a-4df0-b346-4beeb3ca6a0e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boss_falling",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "59f8b60d-0bc6-44e2-b1ee-b435d0a4287f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc73719e-1f5a-4df0-b346-4beeb3ca6a0e",
            "compositeImage": {
                "id": "fed00ce7-9ccf-4f99-86bc-0c33405cf808",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59f8b60d-0bc6-44e2-b1ee-b435d0a4287f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f4d2d48-37da-4170-8c8a-8154b28bddb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59f8b60d-0bc6-44e2-b1ee-b435d0a4287f",
                    "LayerId": "eec1286d-20b9-4c35-b23e-72a87e9dc690"
                }
            ]
        },
        {
            "id": "ed978eed-1afa-45b6-953e-7c0be319241c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc73719e-1f5a-4df0-b346-4beeb3ca6a0e",
            "compositeImage": {
                "id": "d3ea456d-bc36-430a-bd58-482e980579b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed978eed-1afa-45b6-953e-7c0be319241c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56ee7766-439d-4669-964a-5e02ceb09e86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed978eed-1afa-45b6-953e-7c0be319241c",
                    "LayerId": "eec1286d-20b9-4c35-b23e-72a87e9dc690"
                }
            ]
        },
        {
            "id": "c201b46f-5636-4180-ba3a-162a21d37d51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc73719e-1f5a-4df0-b346-4beeb3ca6a0e",
            "compositeImage": {
                "id": "5293fc4e-dc45-4936-a173-82ff7a0e8d2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c201b46f-5636-4180-ba3a-162a21d37d51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7f9aa1f-0388-4e04-95d2-e90b49c32cc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c201b46f-5636-4180-ba3a-162a21d37d51",
                    "LayerId": "eec1286d-20b9-4c35-b23e-72a87e9dc690"
                }
            ]
        },
        {
            "id": "86f921e5-2b57-42fb-b0db-c93776aa3848",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc73719e-1f5a-4df0-b346-4beeb3ca6a0e",
            "compositeImage": {
                "id": "f7832eda-e68e-4a01-b9f1-a2d5ee9dd893",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86f921e5-2b57-42fb-b0db-c93776aa3848",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e467d9d-8bfd-403e-aba8-15964912a0d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86f921e5-2b57-42fb-b0db-c93776aa3848",
                    "LayerId": "eec1286d-20b9-4c35-b23e-72a87e9dc690"
                }
            ]
        },
        {
            "id": "f13fe0af-cfed-423b-8ef2-527fe89acd13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc73719e-1f5a-4df0-b346-4beeb3ca6a0e",
            "compositeImage": {
                "id": "283fbb10-65f9-41c4-bde2-26372c036ba1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f13fe0af-cfed-423b-8ef2-527fe89acd13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14a72c7d-1a2c-4a67-9cc8-11dc0ef14272",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f13fe0af-cfed-423b-8ef2-527fe89acd13",
                    "LayerId": "eec1286d-20b9-4c35-b23e-72a87e9dc690"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "eec1286d-20b9-4c35-b23e-72a87e9dc690",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cc73719e-1f5a-4df0-b346-4beeb3ca6a0e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 14
}