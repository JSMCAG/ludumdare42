{
    "id": "c839cf91-f704-4d56-a0da-9322941b13a4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hole",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2926458a-daf8-4157-b359-6ffcf80b870c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c839cf91-f704-4d56-a0da-9322941b13a4",
            "compositeImage": {
                "id": "b2d2cef2-a2d8-4242-a7ec-ab1d64ac02ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2926458a-daf8-4157-b359-6ffcf80b870c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5274593-1638-49d9-be64-80aee3a328f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2926458a-daf8-4157-b359-6ffcf80b870c",
                    "LayerId": "c8f3272a-e74e-4a1e-ac8a-96a67f1a7029"
                }
            ]
        },
        {
            "id": "c8bb5b33-905b-4a1e-ad89-f25c83ed51d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c839cf91-f704-4d56-a0da-9322941b13a4",
            "compositeImage": {
                "id": "76b86bd4-ea0f-4f27-9a74-60967ac0d95d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8bb5b33-905b-4a1e-ad89-f25c83ed51d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce2ffa72-a811-4c28-a0ae-6a339ee4c54d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8bb5b33-905b-4a1e-ad89-f25c83ed51d0",
                    "LayerId": "c8f3272a-e74e-4a1e-ac8a-96a67f1a7029"
                }
            ]
        },
        {
            "id": "8b74dc6a-66dc-4c5f-a471-3ecdb7d6626a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c839cf91-f704-4d56-a0da-9322941b13a4",
            "compositeImage": {
                "id": "fff8bd24-eebc-4ec2-a7aa-2bc0a8080110",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b74dc6a-66dc-4c5f-a471-3ecdb7d6626a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b66d68b0-0ab0-4a89-a888-016168939f34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b74dc6a-66dc-4c5f-a471-3ecdb7d6626a",
                    "LayerId": "c8f3272a-e74e-4a1e-ac8a-96a67f1a7029"
                }
            ]
        },
        {
            "id": "526064e4-30eb-452d-b467-b8582883f057",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c839cf91-f704-4d56-a0da-9322941b13a4",
            "compositeImage": {
                "id": "60b7e1e6-4435-4cfa-ba75-589bdd10476d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "526064e4-30eb-452d-b467-b8582883f057",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7359d4fd-31cf-4561-8df6-e9574d381e77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "526064e4-30eb-452d-b467-b8582883f057",
                    "LayerId": "c8f3272a-e74e-4a1e-ac8a-96a67f1a7029"
                }
            ]
        },
        {
            "id": "9256536b-7dcc-4f3d-8da9-06ed710c2b5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c839cf91-f704-4d56-a0da-9322941b13a4",
            "compositeImage": {
                "id": "a7690f82-0a14-497e-af9d-849d5ce0305e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9256536b-7dcc-4f3d-8da9-06ed710c2b5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14c1ab07-083f-445c-8e34-70b301591539",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9256536b-7dcc-4f3d-8da9-06ed710c2b5f",
                    "LayerId": "c8f3272a-e74e-4a1e-ac8a-96a67f1a7029"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "c8f3272a-e74e-4a1e-ac8a-96a67f1a7029",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c839cf91-f704-4d56-a0da-9322941b13a4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}