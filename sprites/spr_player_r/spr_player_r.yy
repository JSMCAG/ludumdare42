{
    "id": "d66079c5-3df8-4ac2-b215-722194b83545",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_r",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d1301804-a8e4-45d1-9cc9-a5e1ef4e7f95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d66079c5-3df8-4ac2-b215-722194b83545",
            "compositeImage": {
                "id": "824e6dae-6775-4f42-a552-45e2c9250598",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1301804-a8e4-45d1-9cc9-a5e1ef4e7f95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "816179c9-7746-4660-873f-667bbcbc538d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1301804-a8e4-45d1-9cc9-a5e1ef4e7f95",
                    "LayerId": "f13d6862-9d4d-478d-be1f-dd337cde17a5"
                }
            ]
        },
        {
            "id": "d431dc73-305b-43f1-ae40-5150b54798c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d66079c5-3df8-4ac2-b215-722194b83545",
            "compositeImage": {
                "id": "711919aa-4c71-4c17-b877-7151e18c5348",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d431dc73-305b-43f1-ae40-5150b54798c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f43d186-4e78-40a3-983b-20ef64d1a714",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d431dc73-305b-43f1-ae40-5150b54798c5",
                    "LayerId": "f13d6862-9d4d-478d-be1f-dd337cde17a5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f13d6862-9d4d-478d-be1f-dd337cde17a5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d66079c5-3df8-4ac2-b215-722194b83545",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 14
}