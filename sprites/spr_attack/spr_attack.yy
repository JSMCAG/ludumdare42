{
    "id": "4494bf37-f2ba-4d4c-9ea6-9dc8126adc2f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "100b1d5b-dba5-4cd6-91e3-a890de510c9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4494bf37-f2ba-4d4c-9ea6-9dc8126adc2f",
            "compositeImage": {
                "id": "fc1a3d44-de9c-465b-92e6-828787d2c7f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "100b1d5b-dba5-4cd6-91e3-a890de510c9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99588d5f-370a-482c-bf02-29b7c1553abc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "100b1d5b-dba5-4cd6-91e3-a890de510c9f",
                    "LayerId": "700c52cc-9ed3-46d1-b00a-a4cd8b22493a"
                }
            ]
        },
        {
            "id": "4280cda8-72f3-4f38-80d2-deae6679c6a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4494bf37-f2ba-4d4c-9ea6-9dc8126adc2f",
            "compositeImage": {
                "id": "865e10ce-a6a8-4a8b-b631-6e16fc19765d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4280cda8-72f3-4f38-80d2-deae6679c6a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6862aa01-aee9-4732-a864-60f033391d9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4280cda8-72f3-4f38-80d2-deae6679c6a0",
                    "LayerId": "700c52cc-9ed3-46d1-b00a-a4cd8b22493a"
                }
            ]
        },
        {
            "id": "6b233705-8fa7-4d8e-8763-7b6ee63cabde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4494bf37-f2ba-4d4c-9ea6-9dc8126adc2f",
            "compositeImage": {
                "id": "4d0c4a7f-9f85-4f09-bd31-adc1059cf422",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b233705-8fa7-4d8e-8763-7b6ee63cabde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acb28b4a-1f51-4536-b570-a36d49912d3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b233705-8fa7-4d8e-8763-7b6ee63cabde",
                    "LayerId": "700c52cc-9ed3-46d1-b00a-a4cd8b22493a"
                }
            ]
        },
        {
            "id": "5c3efa09-82da-4066-9e1a-dd9738ec1ee3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4494bf37-f2ba-4d4c-9ea6-9dc8126adc2f",
            "compositeImage": {
                "id": "be787010-3924-4c76-9a4b-c598a938fc7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c3efa09-82da-4066-9e1a-dd9738ec1ee3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83bdfe9b-f054-46e2-8c5c-c2d4dfbde216",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c3efa09-82da-4066-9e1a-dd9738ec1ee3",
                    "LayerId": "700c52cc-9ed3-46d1-b00a-a4cd8b22493a"
                }
            ]
        },
        {
            "id": "d4a173de-08d4-499b-ad27-f2e3ae9eae22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4494bf37-f2ba-4d4c-9ea6-9dc8126adc2f",
            "compositeImage": {
                "id": "cb4766c0-82aa-4877-a6b9-a2ee682fbb30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4a173de-08d4-499b-ad27-f2e3ae9eae22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9758d078-ae8c-4f94-ab73-18e186de3632",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4a173de-08d4-499b-ad27-f2e3ae9eae22",
                    "LayerId": "700c52cc-9ed3-46d1-b00a-a4cd8b22493a"
                }
            ]
        },
        {
            "id": "45b3dca3-eadf-42d0-9fd0-38863abcc026",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4494bf37-f2ba-4d4c-9ea6-9dc8126adc2f",
            "compositeImage": {
                "id": "1f25d3e2-b464-48e0-ada6-c7a5e641c83f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45b3dca3-eadf-42d0-9fd0-38863abcc026",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43ebf0c5-0887-48db-af0e-8eefd2d4aa74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45b3dca3-eadf-42d0-9fd0-38863abcc026",
                    "LayerId": "700c52cc-9ed3-46d1-b00a-a4cd8b22493a"
                }
            ]
        },
        {
            "id": "2dff4c3c-bd28-46a0-ae2a-c5a153789ff4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4494bf37-f2ba-4d4c-9ea6-9dc8126adc2f",
            "compositeImage": {
                "id": "116f5205-fc30-497a-b4cc-07d48a606a31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dff4c3c-bd28-46a0-ae2a-c5a153789ff4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92792bc4-c2f7-4b56-ab0a-4c92816f49e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dff4c3c-bd28-46a0-ae2a-c5a153789ff4",
                    "LayerId": "700c52cc-9ed3-46d1-b00a-a4cd8b22493a"
                }
            ]
        },
        {
            "id": "b7464eb8-eab0-4580-a3f0-d055018e52a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4494bf37-f2ba-4d4c-9ea6-9dc8126adc2f",
            "compositeImage": {
                "id": "e1651cf7-3030-48fe-aab9-140f30d94c68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7464eb8-eab0-4580-a3f0-d055018e52a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e10ac90a-487d-4cd8-af3c-3b45d74ad0be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7464eb8-eab0-4580-a3f0-d055018e52a2",
                    "LayerId": "700c52cc-9ed3-46d1-b00a-a4cd8b22493a"
                }
            ]
        },
        {
            "id": "a8d36c7f-af43-4c57-9f27-ded036e5c6fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4494bf37-f2ba-4d4c-9ea6-9dc8126adc2f",
            "compositeImage": {
                "id": "ed84201f-b028-4abd-bdc1-a8cdbffa6b19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8d36c7f-af43-4c57-9f27-ded036e5c6fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bccd90c-438e-446d-a4f7-cff2af421400",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8d36c7f-af43-4c57-9f27-ded036e5c6fc",
                    "LayerId": "700c52cc-9ed3-46d1-b00a-a4cd8b22493a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "700c52cc-9ed3-46d1-b00a-a4cd8b22493a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4494bf37-f2ba-4d4c-9ea6-9dc8126adc2f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 8
}