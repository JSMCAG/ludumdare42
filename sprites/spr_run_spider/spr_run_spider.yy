{
    "id": "8d76a162-60dc-402d-88ac-606430ac05be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_run_spider",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9376e3b2-380c-4355-9e9e-0455ba827a48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d76a162-60dc-402d-88ac-606430ac05be",
            "compositeImage": {
                "id": "418b57f2-27a3-473e-aa0a-2bb58aa9107a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9376e3b2-380c-4355-9e9e-0455ba827a48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f906415f-83a9-4878-b2a5-85b63b186aab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9376e3b2-380c-4355-9e9e-0455ba827a48",
                    "LayerId": "68601359-232c-4cd6-91fb-d4a6e4cc2e4c"
                }
            ]
        },
        {
            "id": "b1eb840d-7453-494c-8128-7495bb5dbc9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d76a162-60dc-402d-88ac-606430ac05be",
            "compositeImage": {
                "id": "869fed83-a452-47a9-877e-9c75be397c66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1eb840d-7453-494c-8128-7495bb5dbc9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4466a2b4-db6f-477f-b17a-d2aa8088f1ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1eb840d-7453-494c-8128-7495bb5dbc9f",
                    "LayerId": "68601359-232c-4cd6-91fb-d4a6e4cc2e4c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "68601359-232c-4cd6-91fb-d4a6e4cc2e4c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8d76a162-60dc-402d-88ac-606430ac05be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}