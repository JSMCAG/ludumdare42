{
    "id": "4a7472b6-86ba-4e6e-8d61-a95e039348e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_attacking_l",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "827082f5-a80e-4e07-ba66-86b46e211d91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a7472b6-86ba-4e6e-8d61-a95e039348e6",
            "compositeImage": {
                "id": "44260313-3b3b-42f0-b156-52e3209cc8f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "827082f5-a80e-4e07-ba66-86b46e211d91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa67c503-f2b9-487e-b2dd-4df66518e280",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "827082f5-a80e-4e07-ba66-86b46e211d91",
                    "LayerId": "ea5c5dae-f207-4fbf-8cc3-a67e11869f29"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ea5c5dae-f207-4fbf-8cc3-a67e11869f29",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4a7472b6-86ba-4e6e-8d61-a95e039348e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 14
}