{
    "id": "897c4350-71ba-4b00-a4ae-f66ea6befcb6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_l",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2dc9614d-68df-416c-aec5-2f16df32ec1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "897c4350-71ba-4b00-a4ae-f66ea6befcb6",
            "compositeImage": {
                "id": "f598512a-1c14-4632-af50-0fe3e4a6c988",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dc9614d-68df-416c-aec5-2f16df32ec1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb5b09e2-4590-41a0-8953-66cf5eab9a48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dc9614d-68df-416c-aec5-2f16df32ec1f",
                    "LayerId": "4ff212d1-ccce-4ce4-bf05-a448dcf2ed91"
                }
            ]
        },
        {
            "id": "0f5df3f6-6700-4dcb-8080-994453b56adb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "897c4350-71ba-4b00-a4ae-f66ea6befcb6",
            "compositeImage": {
                "id": "56dba587-8322-40a6-818b-f3302c017262",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f5df3f6-6700-4dcb-8080-994453b56adb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85ae6eea-d2f5-4eb7-84a4-bf0241172911",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f5df3f6-6700-4dcb-8080-994453b56adb",
                    "LayerId": "4ff212d1-ccce-4ce4-bf05-a448dcf2ed91"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4ff212d1-ccce-4ce4-bf05-a448dcf2ed91",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "897c4350-71ba-4b00-a4ae-f66ea6befcb6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 14
}