{
    "id": "9a2852a4-c54c-4386-a016-f62761658775",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_spit_rock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "93c01661-aafa-47f1-a538-3d4e14a7aca0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a2852a4-c54c-4386-a016-f62761658775",
            "compositeImage": {
                "id": "5418f989-850f-4306-8daa-0f51bbc717ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93c01661-aafa-47f1-a538-3d4e14a7aca0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dd287f2-4884-4257-ab97-2292fe9fc1b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93c01661-aafa-47f1-a538-3d4e14a7aca0",
                    "LayerId": "7f73d245-ef06-4a47-b637-d59687957d0f"
                }
            ]
        },
        {
            "id": "e513e630-f739-4e5b-b8bc-de186e84d0ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a2852a4-c54c-4386-a016-f62761658775",
            "compositeImage": {
                "id": "358e69c3-3778-400e-a2c5-c592f0795096",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e513e630-f739-4e5b-b8bc-de186e84d0ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb0a8cac-8868-4803-a087-929cd7f6c0b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e513e630-f739-4e5b-b8bc-de186e84d0ba",
                    "LayerId": "7f73d245-ef06-4a47-b637-d59687957d0f"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 8,
    "layers": [
        {
            "id": "7f73d245-ef06-4a47-b637-d59687957d0f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9a2852a4-c54c-4386-a016-f62761658775",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 7
}