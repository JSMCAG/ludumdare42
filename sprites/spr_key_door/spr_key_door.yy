{
    "id": "6a10d448-6fb1-4efb-aff7-f65f1a143ab9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_key_door",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c0b465c2-15db-4bf1-8180-53b4caab80f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a10d448-6fb1-4efb-aff7-f65f1a143ab9",
            "compositeImage": {
                "id": "c2f2f06e-ee0b-49c3-8d3b-0af6e27b4b76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0b465c2-15db-4bf1-8180-53b4caab80f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d202f0ac-b7a6-4961-b75e-12c59b367005",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0b465c2-15db-4bf1-8180-53b4caab80f0",
                    "LayerId": "f96fb157-edb2-4ef6-a65b-4106167d3a76"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f96fb157-edb2-4ef6-a65b-4106167d3a76",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6a10d448-6fb1-4efb-aff7-f65f1a143ab9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 15
}