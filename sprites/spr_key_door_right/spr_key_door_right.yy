{
    "id": "026ccdf9-d46f-4656-8e85-3685259870c5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_key_door_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "029104fe-0578-491c-9d93-57e3ecf5f0cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "026ccdf9-d46f-4656-8e85-3685259870c5",
            "compositeImage": {
                "id": "c9f34927-f927-4061-9a08-1e9dc39f0a58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "029104fe-0578-491c-9d93-57e3ecf5f0cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f784dfb-b236-443d-a499-0e493afc84c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "029104fe-0578-491c-9d93-57e3ecf5f0cb",
                    "LayerId": "4b6937f9-034b-4fe8-a96b-26a6031a8c14"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4b6937f9-034b-4fe8-a96b-26a6031a8c14",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "026ccdf9-d46f-4656-8e85-3685259870c5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 5,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 15,
    "yorig": 8
}