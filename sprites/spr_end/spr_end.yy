{
    "id": "7e4b8b94-b067-4a3c-87c2-e0d83004982c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_end",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1e0cc01d-ecc6-404a-a34f-74eb7c4bf3b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e4b8b94-b067-4a3c-87c2-e0d83004982c",
            "compositeImage": {
                "id": "19bf3b4f-b0f5-4a8a-b7c9-dab82235cc75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e0cc01d-ecc6-404a-a34f-74eb7c4bf3b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac17d543-d0e6-4063-b65a-231fadfa8008",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e0cc01d-ecc6-404a-a34f-74eb7c4bf3b9",
                    "LayerId": "12f9944c-96dd-47b4-8735-b46431ca1757"
                },
                {
                    "id": "0779ae81-f250-448c-b728-e3c95a2c6991",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e0cc01d-ecc6-404a-a34f-74eb7c4bf3b9",
                    "LayerId": "f4dcfb17-c81b-4204-a215-de5f4eb8078d"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 144,
    "layers": [
        {
            "id": "f4dcfb17-c81b-4204-a215-de5f4eb8078d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7e4b8b94-b067-4a3c-87c2-e0d83004982c",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "12f9944c-96dd-47b4-8735-b46431ca1757",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7e4b8b94-b067-4a3c-87c2-e0d83004982c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 0,
    "yorig": 0
}