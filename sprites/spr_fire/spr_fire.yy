{
    "id": "03fc5f55-8a6d-44a3-a18f-19cb080e3e67",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "238da5e8-5488-4b34-896e-02f87556b957",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03fc5f55-8a6d-44a3-a18f-19cb080e3e67",
            "compositeImage": {
                "id": "62df2182-958f-41ed-b50b-9023b9aead8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "238da5e8-5488-4b34-896e-02f87556b957",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ec4d8f6-f719-4f95-9c0f-331f86ef9505",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "238da5e8-5488-4b34-896e-02f87556b957",
                    "LayerId": "8092c0d4-4fa8-4548-b130-422f270718ce"
                }
            ]
        },
        {
            "id": "109fecf1-d5a8-4171-b262-f6e2315639ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03fc5f55-8a6d-44a3-a18f-19cb080e3e67",
            "compositeImage": {
                "id": "145018ef-9325-44ef-80b8-5a0228d59619",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "109fecf1-d5a8-4171-b262-f6e2315639ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51ac4966-00a9-4ccb-ae0a-ebac2dfb1c11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "109fecf1-d5a8-4171-b262-f6e2315639ed",
                    "LayerId": "8092c0d4-4fa8-4548-b130-422f270718ce"
                }
            ]
        },
        {
            "id": "7c8ce8d0-d21d-4b51-8d40-f6d6dd43bf96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03fc5f55-8a6d-44a3-a18f-19cb080e3e67",
            "compositeImage": {
                "id": "834b9106-cc25-4bad-a6c5-3831e7262b2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c8ce8d0-d21d-4b51-8d40-f6d6dd43bf96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c102904-ef26-4edc-b742-f8e4b9a67887",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c8ce8d0-d21d-4b51-8d40-f6d6dd43bf96",
                    "LayerId": "8092c0d4-4fa8-4548-b130-422f270718ce"
                }
            ]
        },
        {
            "id": "7df97236-d606-4cc2-9181-000839393818",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03fc5f55-8a6d-44a3-a18f-19cb080e3e67",
            "compositeImage": {
                "id": "a8043bd7-1e6f-41e9-a8de-56050b358447",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7df97236-d606-4cc2-9181-000839393818",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16ba62f8-4e19-47b0-b359-c12120b6a8f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7df97236-d606-4cc2-9181-000839393818",
                    "LayerId": "8092c0d4-4fa8-4548-b130-422f270718ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "8092c0d4-4fa8-4548-b130-422f270718ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "03fc5f55-8a6d-44a3-a18f-19cb080e3e67",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 5,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 7,
    "yorig": 4
}