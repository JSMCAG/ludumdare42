{
    "id": "57f608e2-7f1b-4a07-a7c5-1375b06815a4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_u",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f364d8ff-620b-49c2-97ca-6fe9165d11b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57f608e2-7f1b-4a07-a7c5-1375b06815a4",
            "compositeImage": {
                "id": "9599c294-8e9c-4728-af55-ed94a5134331",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f364d8ff-620b-49c2-97ca-6fe9165d11b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "338aaaf6-6da8-488e-94ac-c968a0304c9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f364d8ff-620b-49c2-97ca-6fe9165d11b5",
                    "LayerId": "cde21fda-9edc-49b1-9306-99c44d7305b4"
                }
            ]
        },
        {
            "id": "23b9feb3-fe39-4cd1-935b-e6824487aced",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57f608e2-7f1b-4a07-a7c5-1375b06815a4",
            "compositeImage": {
                "id": "b31f85d7-6ded-4281-927b-cf6a2f12e84e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23b9feb3-fe39-4cd1-935b-e6824487aced",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a75056a-7724-4bc9-93c0-fe9ca7de5b12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23b9feb3-fe39-4cd1-935b-e6824487aced",
                    "LayerId": "cde21fda-9edc-49b1-9306-99c44d7305b4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "cde21fda-9edc-49b1-9306-99c44d7305b4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "57f608e2-7f1b-4a07-a7c5-1375b06815a4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 14
}