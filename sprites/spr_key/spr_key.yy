{
    "id": "5f7d1027-3e01-4cb4-bf83-90074a22f1ab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_key",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 1,
    "bbox_right": 6,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7ecb205f-ecdb-4eac-ab7b-bd7eaa6bcfde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f7d1027-3e01-4cb4-bf83-90074a22f1ab",
            "compositeImage": {
                "id": "95bd8551-fcd2-4a35-bc4d-96cec28c1321",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ecb205f-ecdb-4eac-ab7b-bd7eaa6bcfde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39ee4dfb-c337-4ee5-84a2-288b9bdedd1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ecb205f-ecdb-4eac-ab7b-bd7eaa6bcfde",
                    "LayerId": "c13919de-8d54-4561-8b99-395ca08ccff9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "c13919de-8d54-4561-8b99-395ca08ccff9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5f7d1027-3e01-4cb4-bf83-90074a22f1ab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}