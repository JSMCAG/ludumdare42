{
    "id": "a8b4bbf1-10df-4def-afe1-f2a4f01263ab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_title",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "46c0b687-b96f-40b5-a3b3-7f014bd855bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8b4bbf1-10df-4def-afe1-f2a4f01263ab",
            "compositeImage": {
                "id": "d8aaebf1-e400-440c-abf2-87d8cd068f89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46c0b687-b96f-40b5-a3b3-7f014bd855bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e33395d-fe70-4c5d-8bea-52cb021c0749",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46c0b687-b96f-40b5-a3b3-7f014bd855bd",
                    "LayerId": "38affbe1-d374-4985-ab38-198017db0c19"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 144,
    "layers": [
        {
            "id": "38affbe1-d374-4985-ab38-198017db0c19",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a8b4bbf1-10df-4def-afe1-f2a4f01263ab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 0,
    "yorig": 0
}