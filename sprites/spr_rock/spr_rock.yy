{
    "id": "1feb632c-94fd-4fc4-a2d1-9b9ea667723c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a452b035-436c-421b-8e82-2fabe55707bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1feb632c-94fd-4fc4-a2d1-9b9ea667723c",
            "compositeImage": {
                "id": "66435561-6b92-46ed-ab8e-d4ad276248d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a452b035-436c-421b-8e82-2fabe55707bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85c65311-0625-49e6-8776-d45142fe3420",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a452b035-436c-421b-8e82-2fabe55707bb",
                    "LayerId": "87432ac0-b023-433f-b6de-37eee7920f87"
                }
            ]
        },
        {
            "id": "ecf74761-da1a-420d-9e2f-18f278b203cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1feb632c-94fd-4fc4-a2d1-9b9ea667723c",
            "compositeImage": {
                "id": "e950d04c-e9c1-412b-b704-2769921ff29b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecf74761-da1a-420d-9e2f-18f278b203cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50feb479-db87-43d9-99c1-c044cb25cbd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecf74761-da1a-420d-9e2f-18f278b203cd",
                    "LayerId": "87432ac0-b023-433f-b6de-37eee7920f87"
                }
            ]
        },
        {
            "id": "94561da1-6e41-40fc-9f71-140f618c9d7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1feb632c-94fd-4fc4-a2d1-9b9ea667723c",
            "compositeImage": {
                "id": "5f4f16c5-dfd2-444a-947f-d1facc9389ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94561da1-6e41-40fc-9f71-140f618c9d7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a7d5822-8d16-4198-9c50-cc6ae9b549d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94561da1-6e41-40fc-9f71-140f618c9d7d",
                    "LayerId": "87432ac0-b023-433f-b6de-37eee7920f87"
                }
            ]
        },
        {
            "id": "6d89fdf4-2e51-48b4-82fa-cb9c679e819a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1feb632c-94fd-4fc4-a2d1-9b9ea667723c",
            "compositeImage": {
                "id": "fd80f7d0-71ee-4b79-b483-0c8406c34e4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d89fdf4-2e51-48b4-82fa-cb9c679e819a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7fde7ff-af8e-46ef-9a24-f6bb488ebd5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d89fdf4-2e51-48b4-82fa-cb9c679e819a",
                    "LayerId": "87432ac0-b023-433f-b6de-37eee7920f87"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "87432ac0-b023-433f-b6de-37eee7920f87",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1feb632c-94fd-4fc4-a2d1-9b9ea667723c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}