{
    "id": "c3ce1e49-7df5-4665-9655-48cdd2a67993",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_big_brute_falling",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 19,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "568f770b-ccd9-4981-b6d5-e3de83a85a95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3ce1e49-7df5-4665-9655-48cdd2a67993",
            "compositeImage": {
                "id": "7fdd85ca-7138-4eb1-9600-55729b006aa7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "568f770b-ccd9-4981-b6d5-e3de83a85a95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89d97ab9-0921-4311-a7c3-e46224940587",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "568f770b-ccd9-4981-b6d5-e3de83a85a95",
                    "LayerId": "bf535226-37a5-4fb9-8524-da0a393c9880"
                }
            ]
        },
        {
            "id": "e1847a55-91b3-479b-ab69-30f2034abbd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3ce1e49-7df5-4665-9655-48cdd2a67993",
            "compositeImage": {
                "id": "329a672a-33f0-4f5a-9d66-e92d32326ead",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1847a55-91b3-479b-ab69-30f2034abbd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce7883f2-0a64-49f4-8acd-ece8a110aa85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1847a55-91b3-479b-ab69-30f2034abbd5",
                    "LayerId": "bf535226-37a5-4fb9-8524-da0a393c9880"
                }
            ]
        },
        {
            "id": "74b25934-cab8-4b85-9866-012c8973ca1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3ce1e49-7df5-4665-9655-48cdd2a67993",
            "compositeImage": {
                "id": "316db332-597a-4e70-baf4-50b9a5138c22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74b25934-cab8-4b85-9866-012c8973ca1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3a91d7c-c711-4318-a905-2db3974ef41f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74b25934-cab8-4b85-9866-012c8973ca1e",
                    "LayerId": "bf535226-37a5-4fb9-8524-da0a393c9880"
                }
            ]
        },
        {
            "id": "9153c4ff-46a5-47c8-838d-b094fbd2170d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3ce1e49-7df5-4665-9655-48cdd2a67993",
            "compositeImage": {
                "id": "d150b437-9ae1-48a1-8a57-2093092eb9bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9153c4ff-46a5-47c8-838d-b094fbd2170d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8a2ea63-39c8-4384-869a-254a94c44232",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9153c4ff-46a5-47c8-838d-b094fbd2170d",
                    "LayerId": "bf535226-37a5-4fb9-8524-da0a393c9880"
                }
            ]
        },
        {
            "id": "554297bd-3a1d-44ad-8da0-6963bbb9348d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3ce1e49-7df5-4665-9655-48cdd2a67993",
            "compositeImage": {
                "id": "2c2f8bc3-e963-4a59-acfa-7fe0b8748940",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "554297bd-3a1d-44ad-8da0-6963bbb9348d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fcb7d39-76d8-44d8-acbb-d71c06fa0822",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "554297bd-3a1d-44ad-8da0-6963bbb9348d",
                    "LayerId": "bf535226-37a5-4fb9-8524-da0a393c9880"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bf535226-37a5-4fb9-8524-da0a393c9880",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c3ce1e49-7df5-4665-9655-48cdd2a67993",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 31
}