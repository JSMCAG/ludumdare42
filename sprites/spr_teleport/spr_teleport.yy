{
    "id": "dd84f7a2-bb1d-496f-961a-d5ce513ca3be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_teleport",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b58bc0b7-4aa5-4368-9db9-2c193d6b3746",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd84f7a2-bb1d-496f-961a-d5ce513ca3be",
            "compositeImage": {
                "id": "b9a632cd-c385-44b0-9614-f26ce17ef92f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b58bc0b7-4aa5-4368-9db9-2c193d6b3746",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac7e6a72-2020-4d98-937c-c9b9d0c1ff04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b58bc0b7-4aa5-4368-9db9-2c193d6b3746",
                    "LayerId": "62edefa4-9ab5-4f7b-b2e2-dc3ddf45de49"
                }
            ]
        },
        {
            "id": "5dde9c97-6e7f-409b-bc3c-2ce515ce10c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd84f7a2-bb1d-496f-961a-d5ce513ca3be",
            "compositeImage": {
                "id": "3bb50482-2aef-4734-9201-b35c766d9a53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5dde9c97-6e7f-409b-bc3c-2ce515ce10c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b131dac-27f9-4bf8-9cd3-a7719c642d11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5dde9c97-6e7f-409b-bc3c-2ce515ce10c9",
                    "LayerId": "62edefa4-9ab5-4f7b-b2e2-dc3ddf45de49"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "62edefa4-9ab5-4f7b-b2e2-dc3ddf45de49",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd84f7a2-bb1d-496f-961a-d5ce513ca3be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}