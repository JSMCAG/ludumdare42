{
    "id": "387eb3a8-96f8-4c18-aaca-52987ed73583",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_attacking_u",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "301bfa07-86a6-49fa-875a-04ee3b749fc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "387eb3a8-96f8-4c18-aaca-52987ed73583",
            "compositeImage": {
                "id": "a02f66d2-d10c-4703-8a2e-dc7d801b75d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "301bfa07-86a6-49fa-875a-04ee3b749fc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "550f80de-81a7-4dff-a051-63faabad63c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "301bfa07-86a6-49fa-875a-04ee3b749fc9",
                    "LayerId": "7afecc8b-68a2-4b37-9bf4-e7b32bbc3a54"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7afecc8b-68a2-4b37-9bf4-e7b32bbc3a54",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "387eb3a8-96f8-4c18-aaca-52987ed73583",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 14
}