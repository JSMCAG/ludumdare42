{
    "id": "b314ff42-f53f-4885-8717-acff30fcafd1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dungeon_tileset",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 8,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ca89c04e-4d03-4713-9c0e-76a0f9d5952c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b314ff42-f53f-4885-8717-acff30fcafd1",
            "compositeImage": {
                "id": "574fd18e-7c12-4cdb-9d2b-b06ac99dcb21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca89c04e-4d03-4713-9c0e-76a0f9d5952c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ebef76f-0b6f-47a1-8f35-a7afbb06309f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca89c04e-4d03-4713-9c0e-76a0f9d5952c",
                    "LayerId": "f512005f-0ea9-4f05-a82d-1bcf1c92fc0a"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 64,
    "layers": [
        {
            "id": "f512005f-0ea9-4f05-a82d-1bcf1c92fc0a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b314ff42-f53f-4885-8717-acff30fcafd1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}