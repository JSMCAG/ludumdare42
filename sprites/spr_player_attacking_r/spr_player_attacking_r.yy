{
    "id": "4d39903b-5835-4b36-8701-6287594989cf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_attacking_r",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5dd9a930-dced-4531-8afc-e1e7e77a3d95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d39903b-5835-4b36-8701-6287594989cf",
            "compositeImage": {
                "id": "81b642f2-09a3-4bd6-865e-221e396287e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5dd9a930-dced-4531-8afc-e1e7e77a3d95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52f0eb01-0139-468c-bc28-279368c439be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5dd9a930-dced-4531-8afc-e1e7e77a3d95",
                    "LayerId": "35dbc3b3-a329-4ee1-a843-fa01e4972ecc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "35dbc3b3-a329-4ee1-a843-fa01e4972ecc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4d39903b-5835-4b36-8701-6287594989cf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 14
}