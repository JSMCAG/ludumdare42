{
    "id": "ae039e49-7f49-4712-a226-2809febf9311",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_falling",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e335472c-7170-4b20-96a9-6b28c95b3a68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae039e49-7f49-4712-a226-2809febf9311",
            "compositeImage": {
                "id": "42a6abba-54bc-4d54-8335-fd254c256b06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e335472c-7170-4b20-96a9-6b28c95b3a68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6ea1fdc-9466-40af-8a5f-a8e659e99a00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e335472c-7170-4b20-96a9-6b28c95b3a68",
                    "LayerId": "9ab3b7f0-194c-47c5-b979-1fd2a191cc94"
                }
            ]
        },
        {
            "id": "27cb6f3a-2ce7-440b-89e6-78fbc3863411",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae039e49-7f49-4712-a226-2809febf9311",
            "compositeImage": {
                "id": "27748a05-854c-44ed-afc3-84642104e1a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27cb6f3a-2ce7-440b-89e6-78fbc3863411",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a59fe4b3-d449-4538-b5bd-e1f644d898e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27cb6f3a-2ce7-440b-89e6-78fbc3863411",
                    "LayerId": "9ab3b7f0-194c-47c5-b979-1fd2a191cc94"
                }
            ]
        },
        {
            "id": "7f1fd9cf-7bb9-4e92-ade4-db5cf7840e59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae039e49-7f49-4712-a226-2809febf9311",
            "compositeImage": {
                "id": "f853adc2-3e70-48d0-b97a-d8fc6b706e8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f1fd9cf-7bb9-4e92-ade4-db5cf7840e59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d57cd62b-7171-42f8-bbc4-af1a8ca3dced",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f1fd9cf-7bb9-4e92-ade4-db5cf7840e59",
                    "LayerId": "9ab3b7f0-194c-47c5-b979-1fd2a191cc94"
                }
            ]
        },
        {
            "id": "504ba79e-fa46-4c47-bfc7-8ae5cb72743a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae039e49-7f49-4712-a226-2809febf9311",
            "compositeImage": {
                "id": "c7a6aa06-d45b-4bf0-8031-d124fab1d420",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "504ba79e-fa46-4c47-bfc7-8ae5cb72743a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "506fb749-f965-4d83-94c8-ce91a7944936",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "504ba79e-fa46-4c47-bfc7-8ae5cb72743a",
                    "LayerId": "9ab3b7f0-194c-47c5-b979-1fd2a191cc94"
                }
            ]
        },
        {
            "id": "812b1b20-c375-4bbd-8214-cf2172be549e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae039e49-7f49-4712-a226-2809febf9311",
            "compositeImage": {
                "id": "914d97e6-ba7b-4518-a669-39bc468631d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "812b1b20-c375-4bbd-8214-cf2172be549e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a62925ba-33dc-4f00-ad88-d33a31c39404",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "812b1b20-c375-4bbd-8214-cf2172be549e",
                    "LayerId": "9ab3b7f0-194c-47c5-b979-1fd2a191cc94"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9ab3b7f0-194c-47c5-b979-1fd2a191cc94",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ae039e49-7f49-4712-a226-2809febf9311",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 14
}