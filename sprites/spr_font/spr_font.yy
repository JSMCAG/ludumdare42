{
    "id": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_font",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 6,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d5c09aba-6bd9-4604-be58-9457b15a760f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "4e4192b2-e7d3-4e73-a457-4cfa40811804",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5c09aba-6bd9-4604-be58-9457b15a760f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83fd6020-2358-4dab-b625-79b8e3b4aa3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5c09aba-6bd9-4604-be58-9457b15a760f",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "05772acd-d5d4-4b1f-badb-e915589192d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "491c7377-b1a7-48df-8e97-57ab118973d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05772acd-d5d4-4b1f-badb-e915589192d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "769e86e6-cc9d-4a3d-be05-1a44c43be025",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05772acd-d5d4-4b1f-badb-e915589192d9",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "81f97d65-fd5c-443b-ab65-7ca5f8aa178b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "034559ee-b556-406c-a6b8-d08c200e0f54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81f97d65-fd5c-443b-ab65-7ca5f8aa178b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cc5e903-802d-46f5-923d-7e681d90d7f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81f97d65-fd5c-443b-ab65-7ca5f8aa178b",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "f5cc996d-324a-402a-aa16-0df18406ae9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "a876a5e3-bb8d-46b9-a1de-5314ed4f0fcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5cc996d-324a-402a-aa16-0df18406ae9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f68f9c4a-2550-45b8-8e21-2d53c61329fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5cc996d-324a-402a-aa16-0df18406ae9c",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "5c5724e9-9108-41a2-aeda-94f8d2a793ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "1084ea4d-7b06-4c6a-9a16-354cf87094f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c5724e9-9108-41a2-aeda-94f8d2a793ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dad8cd84-3076-4e92-831d-de5a6767a15a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c5724e9-9108-41a2-aeda-94f8d2a793ca",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "9c74ea5b-ab7b-4a51-bf27-177bbd710dfc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "d7385cee-15ba-4801-bded-9e5bebb1efb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c74ea5b-ab7b-4a51-bf27-177bbd710dfc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73ba2142-8946-4539-8b7c-56db76d07fcf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c74ea5b-ab7b-4a51-bf27-177bbd710dfc",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "599f9968-3480-4b8d-8bdf-166c61345088",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "4d659df3-48fc-4921-882b-742d991dfb5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "599f9968-3480-4b8d-8bdf-166c61345088",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6bfa103-0438-49ef-9c6b-6caf7c6977cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "599f9968-3480-4b8d-8bdf-166c61345088",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "3c4c6c8e-4aa4-4797-a094-242a51cfdd26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "b9cbc6bc-2507-44c4-911b-045ee6c8123d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c4c6c8e-4aa4-4797-a094-242a51cfdd26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21b50aa6-5190-4329-b340-2570079db625",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c4c6c8e-4aa4-4797-a094-242a51cfdd26",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "89ce18b8-85d2-4533-8613-499bb6cddba7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "81979812-7115-46f0-9e7a-22c36bf0c695",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89ce18b8-85d2-4533-8613-499bb6cddba7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1893865b-df96-4e51-82cb-aafeb7235eda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89ce18b8-85d2-4533-8613-499bb6cddba7",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "e1b55046-3cf7-47a3-b5a8-793c04270328",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "b913b500-3fed-49df-9d9b-fe993c907fac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1b55046-3cf7-47a3-b5a8-793c04270328",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16a69d21-db91-4ede-bbff-fd6797625b2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1b55046-3cf7-47a3-b5a8-793c04270328",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "67fe0c01-3858-40ac-85f7-d2996cb4fbff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "11f7ad1a-ad2a-4b4f-b1f4-28320124e296",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67fe0c01-3858-40ac-85f7-d2996cb4fbff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54fa2dfc-0462-40cd-8023-9d043845b691",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67fe0c01-3858-40ac-85f7-d2996cb4fbff",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "d62e5a0c-9987-4609-9029-a4745d28ad90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "9d2cc04f-89cb-4ee2-a7f2-eaa9ba8efac8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d62e5a0c-9987-4609-9029-a4745d28ad90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5792bd5-bfb3-47d4-86d4-816bf5bd4e92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d62e5a0c-9987-4609-9029-a4745d28ad90",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "76fb06a0-ce6a-49cb-8dfe-1f4f9771daef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "b74bd915-224d-46e7-ac47-48d3b0e8102e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76fb06a0-ce6a-49cb-8dfe-1f4f9771daef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3737940a-46f8-4ece-a766-f91fe88c9f7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76fb06a0-ce6a-49cb-8dfe-1f4f9771daef",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "8affcbb0-405b-4346-a3d7-ba24184c72e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "ec3e2877-8270-400a-8706-987d848c74f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8affcbb0-405b-4346-a3d7-ba24184c72e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a711a9b4-e6b5-49c9-ab5c-14111d75c54e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8affcbb0-405b-4346-a3d7-ba24184c72e7",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "8827cd30-26d0-4fd6-9c9d-8bc9b62d5ad4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "2bbeac31-6a59-44fc-9c5f-fe4be304f43e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8827cd30-26d0-4fd6-9c9d-8bc9b62d5ad4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13acd7ba-5309-4f5d-9da4-f16d0611f802",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8827cd30-26d0-4fd6-9c9d-8bc9b62d5ad4",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "4b543b38-0a39-415c-bb06-08dfdb9aee31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "90f12aa5-337f-48a0-b417-3f60f620e8d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b543b38-0a39-415c-bb06-08dfdb9aee31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f59ab9e5-2242-43e4-a25d-458861cf24e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b543b38-0a39-415c-bb06-08dfdb9aee31",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "8666dc2e-5d25-4cc2-99e3-be2cd46fc7d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "06f8b7a1-e789-45e3-864a-8c079c35aaee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8666dc2e-5d25-4cc2-99e3-be2cd46fc7d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49eefb21-db42-41f7-945a-07598c8ef928",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8666dc2e-5d25-4cc2-99e3-be2cd46fc7d4",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "5ab6f4e2-d682-423d-b8a8-7ef9c3f9f03a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "46df504f-4228-4b21-bd3d-2c099e49e35b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ab6f4e2-d682-423d-b8a8-7ef9c3f9f03a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "618a1d49-446f-447b-84ba-85011260c428",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ab6f4e2-d682-423d-b8a8-7ef9c3f9f03a",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "83ae3fa4-17c9-4be6-b676-4481c426d62f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "46e76d38-74b1-4247-bf19-b5a1bcd4a93f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83ae3fa4-17c9-4be6-b676-4481c426d62f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01981971-04e4-422e-aad7-21952fb8e801",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83ae3fa4-17c9-4be6-b676-4481c426d62f",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "4650d19c-7e17-4fad-8121-a10af611af29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "52deae9c-912b-41f8-884c-e7bffd22fec0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4650d19c-7e17-4fad-8121-a10af611af29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "517d047f-133f-4337-807a-155fa59417eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4650d19c-7e17-4fad-8121-a10af611af29",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "811c153d-b7dd-437a-bb99-3581cb24001d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "44a179b2-05f6-4bff-b0a1-4db69e622f0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "811c153d-b7dd-437a-bb99-3581cb24001d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "756e055d-575d-43e6-b209-b8144dcac1d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "811c153d-b7dd-437a-bb99-3581cb24001d",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "65b16387-486e-47cf-bbae-1a19478988c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "0be9bf53-b2e4-4bcf-91a4-a924dc29e571",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65b16387-486e-47cf-bbae-1a19478988c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e7da127-12b0-4a5c-bd68-89c22d834d0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65b16387-486e-47cf-bbae-1a19478988c0",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "5b65e8eb-b4b4-4e86-8f24-143f3601a846",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "729155f3-efed-4dbb-b0d2-34220905a0cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b65e8eb-b4b4-4e86-8f24-143f3601a846",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7bcba23-a890-4a00-8b18-3dac617ef762",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b65e8eb-b4b4-4e86-8f24-143f3601a846",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "ebd7fcc1-2b16-4214-b9dd-7e4ca84937fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "30ba0900-6256-4ab5-8602-e8eb6ee65288",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebd7fcc1-2b16-4214-b9dd-7e4ca84937fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2687c4d-9815-4adb-b874-4414e303f16f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebd7fcc1-2b16-4214-b9dd-7e4ca84937fb",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "7c7465c8-1707-419d-878e-17907aa92fbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "77139da8-cc04-4e65-b485-725c9899e8a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c7465c8-1707-419d-878e-17907aa92fbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f483e18-07fb-4018-8932-51c918497b4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c7465c8-1707-419d-878e-17907aa92fbc",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "bd8c0aa1-12ba-45f5-b30c-a110db28f83f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "90f3860b-cab9-45f4-a262-c2e8d67c6d21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd8c0aa1-12ba-45f5-b30c-a110db28f83f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07ecc6d6-9216-49fe-83a4-ea2739a6a509",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd8c0aa1-12ba-45f5-b30c-a110db28f83f",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "628f7841-fc98-4aba-9a25-a568091e8f8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "89e41cbc-df5b-4f01-addd-8ebb5a14e06f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "628f7841-fc98-4aba-9a25-a568091e8f8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce015139-1c2c-41c0-930a-304e06420c99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "628f7841-fc98-4aba-9a25-a568091e8f8d",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "d5e218c0-dbef-40ce-8a86-d4fb0576c987",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "e39d677e-7c74-4f2e-9b52-5033d5b55ee1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5e218c0-dbef-40ce-8a86-d4fb0576c987",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fd6b8ed-284a-4e38-be5f-6210df94cfd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5e218c0-dbef-40ce-8a86-d4fb0576c987",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "3b819339-2cf7-4d3a-aaf5-4807419c4d67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "f6f14bcd-48f0-4793-b96a-e157fb9f4708",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b819339-2cf7-4d3a-aaf5-4807419c4d67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6329e18-c2c2-417d-a708-9a4a0ff16c1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b819339-2cf7-4d3a-aaf5-4807419c4d67",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "fd0912f8-9955-4399-af4a-6ba72dabdc13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "e6e3a224-6c9f-407f-ba59-4ff53c95a897",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd0912f8-9955-4399-af4a-6ba72dabdc13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0157e27f-8b53-420e-9489-140cfc296e15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd0912f8-9955-4399-af4a-6ba72dabdc13",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "5a2614a8-8e03-457c-8d04-07f7b4ecd542",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "06859dbd-d56c-4052-901c-8cc830bee4f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a2614a8-8e03-457c-8d04-07f7b4ecd542",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20c6971c-1d2d-403c-8eb0-c68bb62bb3fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a2614a8-8e03-457c-8d04-07f7b4ecd542",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "d9e62fd1-9381-4380-a23f-b19bc3cf6a5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "a9d2989d-21f4-4953-b07f-f61a83748a28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9e62fd1-9381-4380-a23f-b19bc3cf6a5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d389962e-01e6-40c1-a7f5-6f0818af172b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9e62fd1-9381-4380-a23f-b19bc3cf6a5f",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "55e87746-bb39-4f02-8cdc-de618d0845ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "f96526eb-9964-430a-8211-92f26e1d9f7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55e87746-bb39-4f02-8cdc-de618d0845ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7995d90-13b2-4b71-a914-1c2990b12b17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55e87746-bb39-4f02-8cdc-de618d0845ae",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "4b6b5707-d3c1-4bb5-a24b-6182657c6c3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "115fd508-bc91-4373-9640-0f6a5a6e3429",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b6b5707-d3c1-4bb5-a24b-6182657c6c3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6353b19-7774-47a5-9704-5cdc57ca970d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b6b5707-d3c1-4bb5-a24b-6182657c6c3f",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "9ec133d4-7599-46d5-b2d0-ae2aa6c43a11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "0e52213f-83d7-4a0d-83ed-137a22c27620",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ec133d4-7599-46d5-b2d0-ae2aa6c43a11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "deca08ee-edb5-4420-96d5-a300dd0c8db2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ec133d4-7599-46d5-b2d0-ae2aa6c43a11",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "835fbb6f-0367-4dd4-8936-c360a13e5b15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "64a7fae0-63c2-4742-8f0a-a6e3f2aefbbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "835fbb6f-0367-4dd4-8936-c360a13e5b15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60787f5b-34c0-45a3-83ae-17a1fc4ce62e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "835fbb6f-0367-4dd4-8936-c360a13e5b15",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "8338731d-16ac-474e-af2c-c72e8dc9d285",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "a6b2db3c-262a-463c-836d-946983cde88d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8338731d-16ac-474e-af2c-c72e8dc9d285",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2f710bf-89b6-4410-b27a-f897fe1ae0c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8338731d-16ac-474e-af2c-c72e8dc9d285",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "9f881f6d-ddad-4179-9790-6afba00c4ccd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "4b03dc6f-0f2a-438e-9cea-2e5bb383f234",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f881f6d-ddad-4179-9790-6afba00c4ccd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e38ede96-4fe8-4e51-99f0-7352a5c28b8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f881f6d-ddad-4179-9790-6afba00c4ccd",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "1ec428bb-1d3e-4aaf-80f1-64af3f586e07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "f1bb1315-df61-4739-b2b1-4bf1011b8367",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ec428bb-1d3e-4aaf-80f1-64af3f586e07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c1d5839-1442-4321-a79a-96fdf8adf032",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ec428bb-1d3e-4aaf-80f1-64af3f586e07",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "706241b3-3721-49f6-9ee0-3e9812f44b4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "d06ac67f-c94c-4258-b0b1-b9e865b4f110",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "706241b3-3721-49f6-9ee0-3e9812f44b4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6852e05a-a404-4970-a891-52adc0616237",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "706241b3-3721-49f6-9ee0-3e9812f44b4c",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "fa0c5bfc-18d7-4214-bd8b-60f11ed01d4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "72019232-8da9-4357-9019-91457d50b222",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa0c5bfc-18d7-4214-bd8b-60f11ed01d4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27c7543f-e940-4118-b40c-2f37cdd2d01a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa0c5bfc-18d7-4214-bd8b-60f11ed01d4b",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "cc566eb6-01ca-499b-a4c8-aa4bcdb16031",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "4b8d915c-5cfa-4bd7-8219-db8f5bb1fb7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc566eb6-01ca-499b-a4c8-aa4bcdb16031",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b65e55c-0c3b-461c-9550-68c8e66a5384",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc566eb6-01ca-499b-a4c8-aa4bcdb16031",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "98c88ef4-856f-4783-a6b3-c74635ee7e82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "25b3dc89-e650-4680-acd1-3258538af253",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98c88ef4-856f-4783-a6b3-c74635ee7e82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c07010b6-4771-42c2-9134-0847e6a54384",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98c88ef4-856f-4783-a6b3-c74635ee7e82",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "67205c6e-f09d-4ff3-88a4-06b9fc148f69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "b0adef2f-ca9d-4f96-b5a5-a4d0d911239c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67205c6e-f09d-4ff3-88a4-06b9fc148f69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cd5e47b-db62-4193-acb4-77537e74f822",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67205c6e-f09d-4ff3-88a4-06b9fc148f69",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "b1e2425d-18e0-4a4c-b1fa-efadf1bca40f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "f0119f47-8b44-44bd-8544-cddefab016a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1e2425d-18e0-4a4c-b1fa-efadf1bca40f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6eb5ed26-42ac-46bb-b896-eb725a5421d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1e2425d-18e0-4a4c-b1fa-efadf1bca40f",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "8051cfee-c2f4-43ae-a54b-bd1c9a8fdbaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "47f37be4-95a9-4f29-9dbb-2114b13a3ceb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8051cfee-c2f4-43ae-a54b-bd1c9a8fdbaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "252abfb5-894d-46bb-be77-34e128558ea5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8051cfee-c2f4-43ae-a54b-bd1c9a8fdbaf",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "cf95c758-9598-4413-bd5a-8918a9db2aa6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "4be08ff8-cd48-4677-914f-55e434777900",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf95c758-9598-4413-bd5a-8918a9db2aa6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8840adf-6ea4-4647-bcbb-4372bc8d9cc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf95c758-9598-4413-bd5a-8918a9db2aa6",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "52bbc8fc-7f17-4323-8e0a-408200393d04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "c046627f-3197-49d3-9b2f-555ce2b4ab1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52bbc8fc-7f17-4323-8e0a-408200393d04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b9bd006-2ee2-4a0c-a0c1-e741b5457b2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52bbc8fc-7f17-4323-8e0a-408200393d04",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "0dd4c76b-e3f6-44c9-87ab-1e9f3b42943f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "08cb285c-e449-4d6e-92c3-c67466713151",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dd4c76b-e3f6-44c9-87ab-1e9f3b42943f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a70cae7-598b-4e82-887f-6edd729774aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dd4c76b-e3f6-44c9-87ab-1e9f3b42943f",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "eea5867f-361e-433d-9683-298ca4485948",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "6ea450c7-b82c-464b-a49b-41c9bfd23756",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eea5867f-361e-433d-9683-298ca4485948",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "485b512c-55fa-48e2-a3ee-f896a57ba7ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eea5867f-361e-433d-9683-298ca4485948",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "2e4ef468-f333-4f2d-8312-8df8f763a38f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "0e645361-acca-4718-81b4-cccc3fb9c7d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e4ef468-f333-4f2d-8312-8df8f763a38f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be581400-a96b-4c43-94c2-77761157d32d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e4ef468-f333-4f2d-8312-8df8f763a38f",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "2a705a7b-a300-4c0e-8670-3e730e641513",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "6348f9b1-0284-421a-b1bf-d2b496e32946",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a705a7b-a300-4c0e-8670-3e730e641513",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ebedc43-147f-4ee7-87fc-1c7de366ea5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a705a7b-a300-4c0e-8670-3e730e641513",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "62ecda0a-63d6-4ab0-ae8e-bb2f638ec775",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "ba1178f1-b57a-4c78-a4ea-764e353cdacb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62ecda0a-63d6-4ab0-ae8e-bb2f638ec775",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc1fc7db-aaca-481f-8a25-6d4f4ddcd738",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62ecda0a-63d6-4ab0-ae8e-bb2f638ec775",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "0c0f0ca8-f39b-4a80-8220-883719590e8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "9f2b7f19-f0e7-43ba-ba94-23ef90a158c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c0f0ca8-f39b-4a80-8220-883719590e8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8019dc66-5833-4407-931d-ac80755d170a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c0f0ca8-f39b-4a80-8220-883719590e8d",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "28648fe1-d672-4ebf-8d18-ae4c4c142b3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "4d867faf-d082-43f4-944c-f02ea818f3e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28648fe1-d672-4ebf-8d18-ae4c4c142b3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38e85c04-38d6-4687-8cac-67ac4f111649",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28648fe1-d672-4ebf-8d18-ae4c4c142b3f",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "82919269-7754-4182-901c-38f685c04ce8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "2592d685-6757-4647-a36a-fd85a33aaaa9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82919269-7754-4182-901c-38f685c04ce8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a07e35df-b753-440f-8af7-04d5518583aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82919269-7754-4182-901c-38f685c04ce8",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "6f00bc88-e0ba-4428-8943-e568c7d99bba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "388f6e19-eea9-4f1a-979c-9456dd1f8339",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f00bc88-e0ba-4428-8943-e568c7d99bba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36b0c3ef-875e-45a9-8fd3-590902bfa835",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f00bc88-e0ba-4428-8943-e568c7d99bba",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "9b28bb0d-5a92-482b-a7d1-ee454e178b43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "c2e0ccb3-017e-42fb-98e5-8b80793b941f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b28bb0d-5a92-482b-a7d1-ee454e178b43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d4f5617-116b-4402-bb57-7aeb1d9f5c68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b28bb0d-5a92-482b-a7d1-ee454e178b43",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        },
        {
            "id": "ec11954a-cb67-4097-8d50-3c83b3b6321f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "compositeImage": {
                "id": "66814279-7dfb-4fb5-b0f9-7227504056be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec11954a-cb67-4097-8d50-3c83b3b6321f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "261dbd31-4ce8-4a63-a208-c6b36014e7c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec11954a-cb67-4097-8d50-3c83b3b6321f",
                    "LayerId": "b5df3b62-5820-4161-a10d-e6f41679fe9b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "b5df3b62-5820-4161-a10d-e6f41679fe9b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "91ff5264-2dbe-4330-a8c4-589baa2bb950",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}