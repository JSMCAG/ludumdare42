{
    "id": "a4fd4aec-6539-4d04-8804-3e367447af26",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_run_spider_teleport",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c44055f7-a6e3-48de-bc66-5169e8d5bc5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd4aec-6539-4d04-8804-3e367447af26",
            "compositeImage": {
                "id": "6a404a00-9268-4c55-8a0b-5f24cc024fad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c44055f7-a6e3-48de-bc66-5169e8d5bc5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4141e5af-c6b0-4f70-9aa3-c68b27842a35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c44055f7-a6e3-48de-bc66-5169e8d5bc5e",
                    "LayerId": "014142e2-5b36-4780-b1b1-1c63f90b09a1"
                }
            ]
        },
        {
            "id": "1e5a35b1-3e9f-4f27-8b2a-af40b5a92505",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd4aec-6539-4d04-8804-3e367447af26",
            "compositeImage": {
                "id": "90d3d851-3e62-4f8c-928c-e01cf346258f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e5a35b1-3e9f-4f27-8b2a-af40b5a92505",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5eed3862-2f2f-40b2-9b39-6801cedfe0fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e5a35b1-3e9f-4f27-8b2a-af40b5a92505",
                    "LayerId": "014142e2-5b36-4780-b1b1-1c63f90b09a1"
                }
            ]
        },
        {
            "id": "a59c6732-f1a7-46d4-bd6d-22506a35e5db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd4aec-6539-4d04-8804-3e367447af26",
            "compositeImage": {
                "id": "fc467739-8add-4c7f-a385-bd288d703301",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a59c6732-f1a7-46d4-bd6d-22506a35e5db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9503af85-90b9-4504-a30d-1bf72b36803f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a59c6732-f1a7-46d4-bd6d-22506a35e5db",
                    "LayerId": "014142e2-5b36-4780-b1b1-1c63f90b09a1"
                }
            ]
        },
        {
            "id": "2d68131f-a92b-4012-980f-5816f6124a25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd4aec-6539-4d04-8804-3e367447af26",
            "compositeImage": {
                "id": "fac5108b-dfbe-4381-b3c0-ab5fc3d57e7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d68131f-a92b-4012-980f-5816f6124a25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7df1ab70-6e5c-46d4-af40-732d9caf831b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d68131f-a92b-4012-980f-5816f6124a25",
                    "LayerId": "014142e2-5b36-4780-b1b1-1c63f90b09a1"
                }
            ]
        },
        {
            "id": "3df6ca05-124b-4f18-94aa-140d45a71031",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4fd4aec-6539-4d04-8804-3e367447af26",
            "compositeImage": {
                "id": "ebf8d149-95cc-47aa-957d-c7ca97fbfe21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3df6ca05-124b-4f18-94aa-140d45a71031",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c04bb32-04ed-45f9-a761-fcef08e01169",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3df6ca05-124b-4f18-94aa-140d45a71031",
                    "LayerId": "014142e2-5b36-4780-b1b1-1c63f90b09a1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "014142e2-5b36-4780-b1b1-1c63f90b09a1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a4fd4aec-6539-4d04-8804-3e367447af26",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}