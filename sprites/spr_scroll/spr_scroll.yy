{
    "id": "0f0ec2d1-af44-47d6-98cb-bfe76de05ccb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_scroll",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c6aacc52-177b-45aa-98ef-594a2b304724",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f0ec2d1-af44-47d6-98cb-bfe76de05ccb",
            "compositeImage": {
                "id": "73d1b355-b8ae-4d24-b6c4-c27ac6a9e7ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6aacc52-177b-45aa-98ef-594a2b304724",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de3a4d3a-70a5-4fda-87f9-03dbf91eeab4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6aacc52-177b-45aa-98ef-594a2b304724",
                    "LayerId": "de5f10e4-2f18-4a02-bdcd-0c80860a2c10"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "de5f10e4-2f18-4a02-bdcd-0c80860a2c10",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f0ec2d1-af44-47d6-98cb-bfe76de05ccb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 7
}