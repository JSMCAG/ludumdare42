{
    "id": "a7abcd07-2be6-4466-a2a7-5445e55e4d78",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boss_disappear",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "58560f7c-5479-4755-bd83-cd56daa057b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7abcd07-2be6-4466-a2a7-5445e55e4d78",
            "compositeImage": {
                "id": "9b79fe08-76f5-44ab-b194-884440c127eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58560f7c-5479-4755-bd83-cd56daa057b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5af9ba66-7cfd-4e30-b11b-9a978db00c07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58560f7c-5479-4755-bd83-cd56daa057b9",
                    "LayerId": "40506d9f-51fe-439f-a7e8-771122ca99bb"
                }
            ]
        },
        {
            "id": "f26caf61-e724-48af-b769-097332670a7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7abcd07-2be6-4466-a2a7-5445e55e4d78",
            "compositeImage": {
                "id": "38cb4081-96d5-4c35-a301-62a55821908b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f26caf61-e724-48af-b769-097332670a7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "daa46e9d-123b-4446-b919-d89dc5e7d295",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f26caf61-e724-48af-b769-097332670a7c",
                    "LayerId": "40506d9f-51fe-439f-a7e8-771122ca99bb"
                }
            ]
        },
        {
            "id": "5a4e9ebd-6b4d-41b3-8a43-171c55e7b147",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7abcd07-2be6-4466-a2a7-5445e55e4d78",
            "compositeImage": {
                "id": "658cf48e-8f6a-4395-91ae-891d07323ed8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a4e9ebd-6b4d-41b3-8a43-171c55e7b147",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6c8cbeb-3c25-4099-a9d3-bdbd880fb202",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a4e9ebd-6b4d-41b3-8a43-171c55e7b147",
                    "LayerId": "40506d9f-51fe-439f-a7e8-771122ca99bb"
                }
            ]
        },
        {
            "id": "b20ff8b9-ff26-46ef-93b3-e72f1163e0f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7abcd07-2be6-4466-a2a7-5445e55e4d78",
            "compositeImage": {
                "id": "05beb2ef-0014-4e62-8a83-141667215856",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b20ff8b9-ff26-46ef-93b3-e72f1163e0f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6f6ee56-fdd3-4e9a-b37a-77bd8d14354c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b20ff8b9-ff26-46ef-93b3-e72f1163e0f9",
                    "LayerId": "40506d9f-51fe-439f-a7e8-771122ca99bb"
                }
            ]
        },
        {
            "id": "ac68c757-b13c-4477-ab93-96b77fd9f304",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7abcd07-2be6-4466-a2a7-5445e55e4d78",
            "compositeImage": {
                "id": "a508692c-b234-40c7-b66a-431d9b5aeefb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac68c757-b13c-4477-ab93-96b77fd9f304",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "648be280-fe0f-4166-816a-7d0ed62cf9a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac68c757-b13c-4477-ab93-96b77fd9f304",
                    "LayerId": "40506d9f-51fe-439f-a7e8-771122ca99bb"
                }
            ]
        },
        {
            "id": "3806c1d3-39e8-4703-953f-327d2045773c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7abcd07-2be6-4466-a2a7-5445e55e4d78",
            "compositeImage": {
                "id": "a84897a0-41b2-4300-84ce-750f62fbe4b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3806c1d3-39e8-4703-953f-327d2045773c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7271d4ca-f6fd-4865-bc9d-ca5d79951073",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3806c1d3-39e8-4703-953f-327d2045773c",
                    "LayerId": "40506d9f-51fe-439f-a7e8-771122ca99bb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "40506d9f-51fe-439f-a7e8-771122ca99bb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a7abcd07-2be6-4466-a2a7-5445e55e4d78",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 14
}