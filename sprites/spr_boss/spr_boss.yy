{
    "id": "09e5189f-ef62-4754-8c7c-20e4c5b1a5da",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boss",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "53fb3e9b-084b-4126-a254-00cae6709fc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e5189f-ef62-4754-8c7c-20e4c5b1a5da",
            "compositeImage": {
                "id": "a814ad85-6a04-48d7-a342-9fe32e1a532f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53fb3e9b-084b-4126-a254-00cae6709fc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ba3d494-a7c9-410f-a0cc-2525060bfd04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53fb3e9b-084b-4126-a254-00cae6709fc8",
                    "LayerId": "1cb639d5-ace5-4f78-8bc0-4423e924cdab"
                }
            ]
        },
        {
            "id": "ff24b833-8ba8-4853-995d-6080584a4fc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e5189f-ef62-4754-8c7c-20e4c5b1a5da",
            "compositeImage": {
                "id": "fc28ec99-3e10-439a-94ac-2faa1b1b5177",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff24b833-8ba8-4853-995d-6080584a4fc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "287d5a55-2b70-4e32-8f20-d3dd63e7fc82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff24b833-8ba8-4853-995d-6080584a4fc5",
                    "LayerId": "1cb639d5-ace5-4f78-8bc0-4423e924cdab"
                }
            ]
        },
        {
            "id": "6339be97-0e3c-41ed-b7cd-a563cc6b6067",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09e5189f-ef62-4754-8c7c-20e4c5b1a5da",
            "compositeImage": {
                "id": "6fc3beec-27ef-41ff-b687-09cb561850f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6339be97-0e3c-41ed-b7cd-a563cc6b6067",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf17eb60-30ef-4661-8153-68bdbbc091dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6339be97-0e3c-41ed-b7cd-a563cc6b6067",
                    "LayerId": "1cb639d5-ace5-4f78-8bc0-4423e924cdab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "1cb639d5-ace5-4f78-8bc0-4423e924cdab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "09e5189f-ef62-4754-8c7c-20e4c5b1a5da",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 14
}