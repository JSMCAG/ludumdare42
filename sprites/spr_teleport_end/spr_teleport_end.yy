{
    "id": "dbb489ea-8cb9-4c3e-9378-a0f2908c7bfc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_teleport_end",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6b8290d9-610f-4fcd-adc4-a8c37094d99b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbb489ea-8cb9-4c3e-9378-a0f2908c7bfc",
            "compositeImage": {
                "id": "5bbfd5b9-b279-46ba-9e86-ddc02fea16fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b8290d9-610f-4fcd-adc4-a8c37094d99b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64efa806-4e73-4881-bf19-ab43b31b356c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b8290d9-610f-4fcd-adc4-a8c37094d99b",
                    "LayerId": "091fd167-0176-443b-b049-e9ed4e26e4f2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "091fd167-0176-443b-b049-e9ed4e26e4f2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dbb489ea-8cb9-4c3e-9378-a0f2908c7bfc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}