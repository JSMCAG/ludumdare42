{
    "id": "bafbb976-e979-482a-adee-f05272325f43",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boss_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0c735354-7432-49fb-b854-e653f8dd161d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bafbb976-e979-482a-adee-f05272325f43",
            "compositeImage": {
                "id": "473a1527-30d3-4973-8fba-436532db9f6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c735354-7432-49fb-b854-e653f8dd161d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1940cc5-5abc-411b-a2f4-74610dc11135",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c735354-7432-49fb-b854-e653f8dd161d",
                    "LayerId": "7ed2301a-6b20-4cfa-b9ca-5a1ad0b4301c"
                }
            ]
        },
        {
            "id": "10f69f78-6c7b-4a96-afde-b9fde4ac06a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bafbb976-e979-482a-adee-f05272325f43",
            "compositeImage": {
                "id": "73105147-03d3-47ac-b998-5e9009fb1b8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10f69f78-6c7b-4a96-afde-b9fde4ac06a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f3fcad7-0efa-4573-94f1-e233431c3246",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10f69f78-6c7b-4a96-afde-b9fde4ac06a9",
                    "LayerId": "7ed2301a-6b20-4cfa-b9ca-5a1ad0b4301c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7ed2301a-6b20-4cfa-b9ca-5a1ad0b4301c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bafbb976-e979-482a-adee-f05272325f43",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 14
}