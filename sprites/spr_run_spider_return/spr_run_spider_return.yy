{
    "id": "86f3a2b5-2f8a-410c-9a70-2f1da289b2c5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_run_spider_return",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "792484fa-8754-4cb9-9ab9-c6ee6e823d18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86f3a2b5-2f8a-410c-9a70-2f1da289b2c5",
            "compositeImage": {
                "id": "ae8da48c-1822-493e-9106-a6ed153e7f42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "792484fa-8754-4cb9-9ab9-c6ee6e823d18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbba39ba-aa17-4878-b1ce-58666a4ef7fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "792484fa-8754-4cb9-9ab9-c6ee6e823d18",
                    "LayerId": "fd1eb6cb-a859-4474-a8f2-f9e12db6c9bb"
                }
            ]
        },
        {
            "id": "4c6f55d5-7a15-4f2c-b262-a08da98aad61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86f3a2b5-2f8a-410c-9a70-2f1da289b2c5",
            "compositeImage": {
                "id": "1273f7ce-a383-4e7c-a52a-78ca6d3f3ea0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c6f55d5-7a15-4f2c-b262-a08da98aad61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6ec89f0-6499-4824-8158-78a5afecb81d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c6f55d5-7a15-4f2c-b262-a08da98aad61",
                    "LayerId": "fd1eb6cb-a859-4474-a8f2-f9e12db6c9bb"
                }
            ]
        },
        {
            "id": "f947b6a6-37de-492f-8674-d418a7554049",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86f3a2b5-2f8a-410c-9a70-2f1da289b2c5",
            "compositeImage": {
                "id": "29ef44d8-aba4-4c00-8c73-c571a75a4361",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f947b6a6-37de-492f-8674-d418a7554049",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47b880ab-a339-4ccf-8c6c-3645130a36d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f947b6a6-37de-492f-8674-d418a7554049",
                    "LayerId": "fd1eb6cb-a859-4474-a8f2-f9e12db6c9bb"
                }
            ]
        },
        {
            "id": "595ea442-1358-4e9c-9344-663937197214",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86f3a2b5-2f8a-410c-9a70-2f1da289b2c5",
            "compositeImage": {
                "id": "c7df6fcd-6dba-4ff6-80c6-3329f8341a35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "595ea442-1358-4e9c-9344-663937197214",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "272a5f6e-4406-49cd-ad9c-4c9f3465b4d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "595ea442-1358-4e9c-9344-663937197214",
                    "LayerId": "fd1eb6cb-a859-4474-a8f2-f9e12db6c9bb"
                }
            ]
        },
        {
            "id": "e90dfdbf-08ad-427c-a503-09d855621f60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86f3a2b5-2f8a-410c-9a70-2f1da289b2c5",
            "compositeImage": {
                "id": "0624876e-70fc-4072-ab95-ed0c2603ef93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e90dfdbf-08ad-427c-a503-09d855621f60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2fa2bc8-2498-4913-8aea-16dd7f6a609c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e90dfdbf-08ad-427c-a503-09d855621f60",
                    "LayerId": "fd1eb6cb-a859-4474-a8f2-f9e12db6c9bb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "fd1eb6cb-a859-4474-a8f2-f9e12db6c9bb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "86f3a2b5-2f8a-410c-9a70-2f1da289b2c5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}