{
    "id": "8473247c-b744-458b-8bf5-eba6f657c63c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_d",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5545fff4-22c2-4c6d-9cd8-4121f9b866db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8473247c-b744-458b-8bf5-eba6f657c63c",
            "compositeImage": {
                "id": "330c68ba-3a27-4059-910e-93605b4fa5af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5545fff4-22c2-4c6d-9cd8-4121f9b866db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e8aa54f-e050-42dc-9c2c-005d344059be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5545fff4-22c2-4c6d-9cd8-4121f9b866db",
                    "LayerId": "8cb131ec-e69f-4722-80c5-c079e9c28819"
                }
            ]
        },
        {
            "id": "a1d2a38f-22a7-4bde-86e3-ca30d66759eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8473247c-b744-458b-8bf5-eba6f657c63c",
            "compositeImage": {
                "id": "48de458b-8514-4a78-a8cb-56cf74e310ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1d2a38f-22a7-4bde-86e3-ca30d66759eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "142821d6-e81f-47c3-92ef-c9c82544c48a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1d2a38f-22a7-4bde-86e3-ca30d66759eb",
                    "LayerId": "8cb131ec-e69f-4722-80c5-c079e9c28819"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "8cb131ec-e69f-4722-80c5-c079e9c28819",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8473247c-b744-458b-8bf5-eba6f657c63c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 14
}