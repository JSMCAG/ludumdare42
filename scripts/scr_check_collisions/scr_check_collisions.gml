/// @function check_collisions(new_x, new_y, obj)
/// @description Checks whether this object's new position colides with anything. If so, places the object in the nearest legal position, else places it in it's desired position.
/// @param {int} new_x The x coordinate of the desired position.
/// @param {int} new_y The y coordinate of the desired position.
/// @param {array} obj The type of objects to check the collision for.

var new_x = argument0;
var new_y = argument1;
var objs;
var n_objs;
if (is_array(argument2)) {
	objs = argument2;
	n_objs = array_length_1d(argument2);
} else {
	objs = [argument2];
	n_objs = 1;
}

var delta_x = new_x - x;
var delta_y = new_y - y;

var dx_abs = abs(delta_x);
var dy_abs = abs(delta_y);

var dx_sign = sign(delta_x);
var dy_sign = sign(delta_y);

for (var i = 0; i < dx_abs; ++i) {
	
	// check for collisions with any object
	var collided = false;
	for (var j = 0; j < n_objs; ++j) {
		if (place_meeting(x + dx_sign, y, objs[j])) {
			collided = true;
			break;
		}
	}
	
	// move if possible
    if (!collided) {
		x += dx_sign;
	} else {
		break;
	}
}

for (var i = 0; i < dy_abs; ++i) {
	
	// check for collisions with any object
	var collided = false;
	for (var j = 0; j < n_objs; ++j) {
		if (place_meeting(x, y + dy_sign, objs[j])) {
			collided = true;
			break;
		}
	}
	
	// move if possible
    if (!collided) {
		y += dy_sign;
	} else {
		break;
	}
}