/// @description Confirm
if (selected == 0) {
	var music = instance_find(obj_music, 0);
	
	if (!music.mute_music) {
		music.mute_music = true;
		items[0] = "MUSIC ON";
	} else {
		music.mute_music = false;
		items[0] = "MUSIC OFF";
		with (music) {
			event_user(0);
		}
	}
}

if (selected == 1) {
	room_goto(rm_title);
}