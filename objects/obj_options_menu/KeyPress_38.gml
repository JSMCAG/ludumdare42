/// @description Select previous item
selected = (selected - 1);
if (selected < 0) {
	selected = N_ITEMS - 1;
}