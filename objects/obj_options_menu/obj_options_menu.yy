{
    "id": "2f0492e7-d61a-4cef-80c7-7ff63a9364b1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_options_menu",
    "eventList": [
        {
            "id": "faa10d34-f31b-4f7b-96b3-d7c787fb81e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2f0492e7-d61a-4cef-80c7-7ff63a9364b1"
        },
        {
            "id": "453eb081-17f7-421c-a8e2-0d0d5cc18dd6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 9,
            "m_owner": "2f0492e7-d61a-4cef-80c7-7ff63a9364b1"
        },
        {
            "id": "94f5bab7-33f7-4890-9c6a-b0890e1541f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 38,
            "eventtype": 9,
            "m_owner": "2f0492e7-d61a-4cef-80c7-7ff63a9364b1"
        },
        {
            "id": "c9550f15-1ea4-4e06-a11f-2edb2950f83f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "2f0492e7-d61a-4cef-80c7-7ff63a9364b1"
        },
        {
            "id": "7d9e6db9-2fd5-4168-8f54-7c40bfc80ef9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 9,
            "m_owner": "2f0492e7-d61a-4cef-80c7-7ff63a9364b1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}