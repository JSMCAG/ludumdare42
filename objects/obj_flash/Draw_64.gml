/// @description 

if (duration > 0) {
	display_set_gui_maximize(3, 3);
	draw_set_font(fnt_default);
	draw_set_halign(fa_center);

	draw_rectangle_color(0, 0, 160, 144, c_white, c_white, c_white, c_white, false);
}