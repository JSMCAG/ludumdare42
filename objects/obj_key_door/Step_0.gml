/// @description Open if player has both scrolls

event_inherited();


if (player_near) {
	var has_keys = progression.n_keys > 0;
	if (has_keys) {
		// TODO play a sound
		progression.n_keys--;
		instance_destroy();
	}
}