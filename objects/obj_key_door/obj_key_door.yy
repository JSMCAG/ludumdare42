{
    "id": "e5f79271-87fb-4017-809f-f0d05057929f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_key_door",
    "eventList": [
        {
            "id": "8ece3797-1442-46c0-acd0-26a94d79b299",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e5f79271-87fb-4017-809f-f0d05057929f"
        },
        {
            "id": "11fbeda0-24e5-4bc5-80f0-b4a161218bbd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e5f79271-87fb-4017-809f-f0d05057929f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "7ca6babd-65a9-4b2a-af96-3de4af89f6a9",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6a10d448-6fb1-4efb-aff7-f65f1a143ab9",
    "visible": true
}