{
    "id": "71a1678e-ac98-482f-8a5b-224d0f69c744",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_teleport",
    "eventList": [
        {
            "id": "063f6bf2-5661-4fa3-a4d6-94bde95099c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "babec998-e324-4134-8cd2-46ed57021d5c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "71a1678e-ac98-482f-8a5b-224d0f69c744"
        },
        {
            "id": "0707ffdd-43e7-46e9-bf42-369a3ee0cc4f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "71a1678e-ac98-482f-8a5b-224d0f69c744"
        },
        {
            "id": "bf8948d4-dafd-40b0-8385-bf16955bdf78",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e55ee1d1-bd19-4aeb-bba9-33580a63ae71",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "71a1678e-ac98-482f-8a5b-224d0f69c744"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "621cac79-9b74-46d3-8173-729f434923d0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "9aac5c60-9ff4-4f88-bb6b-d8a6e3524b0e",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "teleport_to_x",
            "varType": 0
        },
        {
            "id": "47bc4458-d8c4-4fb4-a887-b175dbe45379",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "teleport_to_y",
            "varType": 0
        }
    ],
    "solid": true,
    "spriteId": "dd84f7a2-bb1d-496f-961a-d5ce513ca3be",
    "visible": true
}