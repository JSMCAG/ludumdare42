/// @description Detect player, open if they have key

if (time.paused) {
	image_speed = 0;
	exit;
} else {
	image_speed = anim_speed;
}

player_near = collision_rectangle(x - 16, y + 1, x + 15, y + 8, obj_player, false, true);

if (player_near) {
	player_near_frame_count++;
} else {
	player_near_frame_count = 0;
}