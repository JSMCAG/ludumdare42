{
    "id": "7ca6babd-65a9-4b2a-af96-3de4af89f6a9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_door",
    "eventList": [
        {
            "id": "3cde9ff6-5d45-4e20-961e-076d3c7018eb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7ca6babd-65a9-4b2a-af96-3de4af89f6a9"
        },
        {
            "id": "9fcc47bd-f1fb-496c-8778-ec1a593c6d92",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7ca6babd-65a9-4b2a-af96-3de4af89f6a9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "404ba90f-fe8c-426f-b7c9-653cfd1cf4ad",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "6a10d448-6fb1-4efb-aff7-f65f1a143ab9",
    "visible": true
}