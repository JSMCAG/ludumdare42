{
    "id": "13af61c9-ada9-4c2e-a835-5d215b9e5d31",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hole",
    "eventList": [
        {
            "id": "062d715f-b692-4fb3-964c-852ae83d593c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "13af61c9-ada9-4c2e-a835-5d215b9e5d31"
        },
        {
            "id": "e69de49c-547a-475c-a21f-472ecb253a58",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "13af61c9-ada9-4c2e-a835-5d215b9e5d31"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "621cac79-9b74-46d3-8173-729f434923d0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c839cf91-f704-4d56-a0da-9322941b13a4",
    "visible": true
}