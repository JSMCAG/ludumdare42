/// @description Stop animation once it reaches its end

if (time.paused) {
	image_speed = 0;
	exit;
} else {
	image_speed = anim_speed;
}

if (image_index > image_number - 1) {
	// We have reached the final frame, stop the animation
	image_speed = 0;
}