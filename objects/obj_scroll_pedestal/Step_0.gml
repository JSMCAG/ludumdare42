/// @description Update animation frame counter

if (time.paused) {
	image_speed = 0;
	exit;
} else {
	image_speed = anim_speed;
}

anim_frame = (anim_frame + 1) mod 60;