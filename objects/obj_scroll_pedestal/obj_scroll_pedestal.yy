{
    "id": "ba5d2b31-6bfc-47ac-b763-9673819ae088",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_scroll_pedestal",
    "eventList": [
        {
            "id": "66120193-fe08-4944-a21a-d17f7e1fa606",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ba5d2b31-6bfc-47ac-b763-9673819ae088"
        },
        {
            "id": "5b21a9cb-5313-42e5-ac81-b17e08f2f9e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ba5d2b31-6bfc-47ac-b763-9673819ae088"
        },
        {
            "id": "b9201fa1-3c88-444a-bca0-4a088d534f52",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ba5d2b31-6bfc-47ac-b763-9673819ae088"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "621cac79-9b74-46d3-8173-729f434923d0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "449920a7-215c-4df3-9727-5b25e85eea6e",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "index",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "75474d3a-83b9-467e-b2dd-7a34261a6ae2",
    "visible": true
}