/// @description Draw scroll if player has it
draw_self();

if(progression.got_scrolls[index]) {
	var scroll_x = x;
	var scroll_y = y - 4;
	
	if ((anim_frame >= 19 && anim_frame < 29) || (anim_frame >= 49 && anim_frame < 59)) {
		scroll_y -= 1;
	} else if (anim_frame >= 29 && anim_frame < 49) {
		scroll_y -= 2;
	}
	
	draw_sprite(spr_scroll, 0, scroll_x, scroll_y);
}