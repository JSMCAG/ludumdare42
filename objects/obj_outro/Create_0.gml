/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

text = "With Agharamish's demise, you were able to set free the Great Tome of Light, and thus bring an end to the era of darkness.\n\nEons from now, legends of the Great Wizard Hero will still be told among the people!\n\n<ENTER> to continue";
next_room = rm_end;