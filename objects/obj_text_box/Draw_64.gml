/// @description Draw text

display_set_gui_maximize(3, 3);
draw_set_font(fnt_default);
draw_set_halign(fa_center);

draw_rectangle_color(0, 32, 160, 112, c_white, c_white, c_white, c_white, false);
draw_text_ext(80, 32, string_upper(text), 8, 160);
draw_text_ext(80, 104, string_upper("<ENTER> to continue"), 8, 160);
