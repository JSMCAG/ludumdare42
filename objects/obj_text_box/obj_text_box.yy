{
    "id": "73063a42-06f6-493d-b866-df962de62b85",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_text_box",
    "eventList": [
        {
            "id": "0f15662a-de03-4732-81b2-eb99ca5ec684",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "73063a42-06f6-493d-b866-df962de62b85"
        },
        {
            "id": "5619eab1-87fc-4a7b-b0aa-7a95e166bef4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "73063a42-06f6-493d-b866-df962de62b85"
        },
        {
            "id": "0882968b-31ad-45a4-a941-281f559c6516",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 9,
            "m_owner": "73063a42-06f6-493d-b866-df962de62b85"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "670b81a3-c705-4042-8dc4-1e9a91d2e2d8",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "\"FOOBAR\"",
            "varName": "text",
            "varType": 2
        }
    ],
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}