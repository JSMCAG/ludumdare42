/// @description Draw text

display_set_gui_maximize(3, 3);
draw_set_font(fnt_default);
draw_set_halign(fa_left);

draw_rectangle_color(0, 0, 160, 144, c_white, c_white, c_white, c_white, false);
draw_text_ext(0, text_y, string_upper(text), 8, 160);
