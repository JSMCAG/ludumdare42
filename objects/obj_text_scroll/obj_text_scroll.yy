{
    "id": "472da8fc-48f7-4a42-9d83-a78e27801250",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_text_scroll",
    "eventList": [
        {
            "id": "efecc1df-3b98-4fd5-b4ab-38d4fbc547f6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "472da8fc-48f7-4a42-9d83-a78e27801250"
        },
        {
            "id": "09df4453-1bf8-462a-aae2-1b18a57de9d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "472da8fc-48f7-4a42-9d83-a78e27801250"
        },
        {
            "id": "23f96e1f-25d2-4624-a7fa-0a5fec856e32",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 9,
            "m_owner": "472da8fc-48f7-4a42-9d83-a78e27801250"
        },
        {
            "id": "cab30053-c52d-4875-bbf1-123fe5f292a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "472da8fc-48f7-4a42-9d83-a78e27801250"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}