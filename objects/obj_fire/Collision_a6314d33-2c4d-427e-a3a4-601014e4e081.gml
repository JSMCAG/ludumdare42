/// @description Ricochet

fire_speed = -fire_speed;

var new_x = x;
while (place_meeting(new_x, y, other)) {
	new_x++;
}
x = new_x;