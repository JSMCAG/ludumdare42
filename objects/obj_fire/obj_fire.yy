{
    "id": "55b4e983-525d-48ac-afcd-128ffb85dd5c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fire",
    "eventList": [
        {
            "id": "a667ee65-1031-4374-b919-93680c3aba11",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "55b4e983-525d-48ac-afcd-128ffb85dd5c"
        },
        {
            "id": "e427d3ed-db3e-4ac4-8282-85f8532b82a8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "55b4e983-525d-48ac-afcd-128ffb85dd5c"
        },
        {
            "id": "a6314d33-2c4d-427e-a3a4-601014e4e081",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c350ad13-af32-439c-a715-a4c0c523f3f4",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "55b4e983-525d-48ac-afcd-128ffb85dd5c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "621cac79-9b74-46d3-8173-729f434923d0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "03fc5f55-8a6d-44a3-a18f-19cb080e3e67",
    "visible": true
}