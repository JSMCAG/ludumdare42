/// @description Move

if (time.paused) {
	image_speed = 0;
	exit;
} else {
	image_speed = anim_speed;
}

x += fire_speed;
if (y < target_y) {
	y++;
}

if (ttl-- == 0) {
	instance_destroy();
}