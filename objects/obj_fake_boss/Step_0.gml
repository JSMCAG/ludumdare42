/// @description Boss state machine

if (time.paused) {
	image_speed = 0;
	exit;
} else {
	image_speed = anim_speed;
}

switch (state) {
		
	case boss_states.MOVING:
		sprite_index = spr_fake_shadow;
		
		move_towards_point(next_rand_pos_x, next_rand_pos_y, 1);
		
		if (abs(point_distance(x, y, next_rand_pos_x, next_rand_pos_y)) < 1) {
			speed = 0;
			state = boss_states.END_MOVING;
		}
		break;
	
	case boss_states.END_MOVING:
		sprite_index = spr_boss_appear;
		
		if (image_index > image_number - 1) {
			// We have reached the final frame
			state = boss_states.START_ATTACKING;
		}
		
		break;
		
	case boss_states.START_ATTACKING:
		sprite_index = spr_boss;
		next_rand_pos_x = irandom_range(BOSS_ARENA_X1, BOSS_ARENA_X2);
		next_rand_pos_y = irandom_range(BOSS_ARENA_Y1, BOSS_ARENA_Y2);
		
		if (image_index > image_number - 1) {
			// We have reached the final frame
			state = boss_states.ATTACKING;
		}

		break;
		
	case boss_states.ATTACKING:
		sprite_index = spr_boss_attack;
		state = boss_states.END_ATTACKING;

		break;
		
	case boss_states.END_ATTACKING:
		
		if (image_index > image_number - 1) {
			// We have reached the final frame
			instance_destroy();
		}

		break;
}
/*
	if ((new_x != x) || (new_y != y)) {
		scr_check_collisions(new_x, new_y, obj_wall);

		var new_tile_x = x div 8;
		var new_tile_y = y div 8;
		if ((new_tile_x != last_tile_x) || (new_tile_y != last_tile_y)) {
			instance_create_layer(last_tile_x * 8, last_tile_y * 8, "layer_holes", obj_hole);
			last_tile_x = new_tile_x;
			last_tile_y = new_tile_y;
		}
	
		var hole = instance_position(x, y, obj_hole);
		if (hole != noone) {
			state = player_states.FALLING;
			x = hole.x + 4;
			y = hole.y + 6;
		}
	}
	*/