/// @description Init state machine

// Inherit the parent event
event_inherited();

BOSS_ARENA_X1 = 824;
BOSS_ARENA_Y1 = 888;
BOSS_ARENA_X2 = 936;
BOSS_ARENA_Y2 = 984;


state = boss_states.MOVING;
do {
	next_rand_pos_x = irandom_range(BOSS_ARENA_X1, BOSS_ARENA_X2);
	next_rand_pos_y = irandom_range(BOSS_ARENA_Y1, BOSS_ARENA_Y2);
} until (!position_meeting(next_rand_pos_x, next_rand_pos_y, obj_hole));