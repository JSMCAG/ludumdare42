{
    "id": "49cd9db9-2689-4ef0-b9e7-7f6286fc0163",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fake_boss",
    "eventList": [
        {
            "id": "c7d54781-ed80-44d8-b5a5-bd7f4844d505",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "49cd9db9-2689-4ef0-b9e7-7f6286fc0163"
        },
        {
            "id": "047c1281-706f-48a5-a0eb-70bbb24d6202",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "49cd9db9-2689-4ef0-b9e7-7f6286fc0163"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "621cac79-9b74-46d3-8173-729f434923d0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ad0674c3-9e37-4f66-8ece-e5371c023962",
    "visible": true
}