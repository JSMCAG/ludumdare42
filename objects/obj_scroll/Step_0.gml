/// @description Animation of floating up and down

if (time.paused) {
	image_speed = 0;
	exit;
} else {
	image_speed = anim_speed;
}

if(anim_frame = 19 || anim_frame = 29) {
	y -= 1;
}

if(anim_frame = 49 || anim_frame = 59) {
	y += 1;
}
anim_frame = (anim_frame + 1) mod 60;
