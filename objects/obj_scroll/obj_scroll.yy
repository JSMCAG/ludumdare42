{
    "id": "11b863db-a230-49cf-99cc-49ca4b32a6c1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_scroll",
    "eventList": [
        {
            "id": "d94db807-ad28-4d60-bcf4-55541b80b48b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "11b863db-a230-49cf-99cc-49ca4b32a6c1"
        },
        {
            "id": "c10d4b1c-8d10-41e7-88c2-59b535d6106e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "11b863db-a230-49cf-99cc-49ca4b32a6c1"
        },
        {
            "id": "728d21c9-0047-4c7f-b7e5-902b8f43d993",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "babec998-e324-4134-8cd2-46ed57021d5c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "11b863db-a230-49cf-99cc-49ca4b32a6c1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "621cac79-9b74-46d3-8173-729f434923d0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "c1f50629-60ca-43fc-8a45-0a6f89d30333",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "\"SCROLL OF NULL\"",
            "varName": "name",
            "varType": 2
        },
        {
            "id": "bf365775-6d75-430e-9fa6-456b227f40aa",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "index",
            "varType": 1
        }
    ],
    "solid": false,
    "spriteId": "0f0ec2d1-af44-47d6-98cb-bfe76de05ccb",
    "visible": true
}