/// @description Player collects scroll
// check progression flag
var progression = instance_find(obj_progression, 0);
progression.got_scrolls[index] = true;
var all_scrolls = true;
for (var i = 0; i < progression.N_SCROLLS; ++i) {
    if (!progression.got_scrolls[i]) {
		all_scrolls = false;
		break;
	}
}

// TODO make popup appear
var txt = instance_create_layer(0, 0, "layer_ui", obj_text_box);
txt.text = "YOU GOT THE \n" + name + "!\n\n"
if (all_scrolls) {
	txt.text += "IT IS NOW TIME TO FIGHT AGHARAMISH!"
} else {
	txt.text += "YOU NEED BOTH SCROLLS TO FIGHT AGHARAMISH!"
}

// destroy self
instance_destroy();