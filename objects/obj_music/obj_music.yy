{
    "id": "597881b6-ab42-4de5-b497-fec226cb42ba",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_music",
    "eventList": [
        {
            "id": "632aa146-f235-43d9-98d7-c5f8ed89383c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "597881b6-ab42-4de5-b497-fec226cb42ba"
        },
        {
            "id": "0f95ae87-9857-433c-9722-44ad79e78948",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "597881b6-ab42-4de5-b497-fec226cb42ba"
        },
        {
            "id": "304ab85b-c01e-4033-bf09-7b58b845af7d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "597881b6-ab42-4de5-b497-fec226cb42ba"
        },
        {
            "id": "1f56c2f6-846c-4898-b40d-68a8a41fa486",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "597881b6-ab42-4de5-b497-fec226cb42ba"
        },
        {
            "id": "810a08c5-2e13-4e1f-929c-3166a9fee5ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 7,
            "m_owner": "597881b6-ab42-4de5-b497-fec226cb42ba"
        },
        {
            "id": "27d8ee53-7275-49a9-8d67-9e0c0878e8d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "597881b6-ab42-4de5-b497-fec226cb42ba"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}