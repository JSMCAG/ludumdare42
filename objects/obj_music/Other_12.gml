/// @description Agharamish
if (mute_music) {
	exit;
}

if (current_bgm != noone) {
	audio_stop_sound(current_bgm);
}
current_bgm = mus_boss;
audio_play_sound(mus_boss, 10, true);
