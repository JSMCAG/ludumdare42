/// @description Mute music
if (mute_music && current_bgm != noone) {
	audio_stop_sound(current_bgm);
}