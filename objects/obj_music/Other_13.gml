/// @description Falling
if (mute_music) {
	exit;
}

if (current_bgm != noone) {
	audio_stop_sound(current_bgm);
}
current_bgm = mus_falling;
audio_play_sound(mus_falling, 10, false);
