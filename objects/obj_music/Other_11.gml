/// @description Heartbeat
if (mute_music) {
	exit;
}

if (current_bgm != noone) {
	audio_stop_sound(current_bgm);
}
current_bgm = mus_heartbeat;
audio_play_sound(mus_heartbeat, 10, true);