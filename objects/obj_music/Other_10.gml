/// @description Dungeon
if (mute_music) {
	exit;
}

if (current_bgm != noone) {
	audio_stop_sound(current_bgm);
}
current_bgm = mus_dungeon;
audio_play_sound(mus_dungeon, 10, true);
