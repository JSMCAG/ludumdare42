/// @description All of this object's children must have this piece of code copy pasted

if (time.paused) {
	image_speed = 0;
	exit;
} else {
	image_speed = anim_speed;
}