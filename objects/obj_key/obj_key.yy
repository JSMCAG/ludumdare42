{
    "id": "ba3e4848-8846-44f2-92ec-60c197271bea",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_key",
    "eventList": [
        {
            "id": "1ce86888-1173-4145-8235-f86e14b8cc8d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "babec998-e324-4134-8cd2-46ed57021d5c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ba3e4848-8846-44f2-92ec-60c197271bea"
        },
        {
            "id": "63f76544-7cd4-4530-bc0e-77dd877e4360",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ba3e4848-8846-44f2-92ec-60c197271bea"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5f7d1027-3e01-4cb4-bf83-90074a22f1ab",
    "visible": true
}