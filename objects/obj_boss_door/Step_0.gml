/// @description Open if player has both scrolls

event_inherited();


if (player_near) {
	var has_scrolls = true;
	for (var i = 0; i < progression.N_SCROLLS; ++i) {
	    if (!progression.got_scrolls[i]) {
			has_scrolls = false;
			break;
		}
	}
	if (has_scrolls) {
		// TODO play a sound
		instance_destroy();
	} else if (player_near_frame_count == 1) {
		var txt_box = instance_create_layer(x, y, "layer_ui", obj_text_box);
		txt_box.text = "YOU NEED BOTH SCROLLS TO OPEN THIS DOOR.";
	}
}