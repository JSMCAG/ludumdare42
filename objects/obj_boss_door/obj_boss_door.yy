{
    "id": "bd735a2c-b6bc-463b-b8ae-b51fcb7099d3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_boss_door",
    "eventList": [
        {
            "id": "7f2f423a-57f0-47bf-9280-066cfe8e6c93",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bd735a2c-b6bc-463b-b8ae-b51fcb7099d3"
        },
        {
            "id": "7958ba1d-1281-4b28-b631-5da51ec4ae49",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bd735a2c-b6bc-463b-b8ae-b51fcb7099d3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "7ca6babd-65a9-4b2a-af96-3de4af89f6a9",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a15dae82-f509-4426-844f-d2f6620fb682",
    "visible": true
}