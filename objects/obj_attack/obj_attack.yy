{
    "id": "c350ad13-af32-439c-a715-a4c0c523f3f4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_attack",
    "eventList": [
        {
            "id": "9ff2a20d-5829-4ad3-9814-5832874c6460",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c350ad13-af32-439c-a715-a4c0c523f3f4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "621cac79-9b74-46d3-8173-729f434923d0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4494bf37-f2ba-4d4c-9ea6-9dc8126adc2f",
    "visible": true
}