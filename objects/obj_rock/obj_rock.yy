{
    "id": "9d215e3a-b5fa-4af3-97d6-c94475045238",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_rock",
    "eventList": [
        {
            "id": "9ef21ab9-9eb5-477b-8335-bbc9b0adedef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9d215e3a-b5fa-4af3-97d6-c94475045238"
        },
        {
            "id": "5f40c159-87c6-45c8-a288-9e05896e8a1f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9d215e3a-b5fa-4af3-97d6-c94475045238"
        },
        {
            "id": "84c35236-7d8a-4d69-a48d-e18b80ca8347",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "404ba90f-fe8c-426f-b7c9-653cfd1cf4ad",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "9d215e3a-b5fa-4af3-97d6-c94475045238"
        },
        {
            "id": "9062f9f3-824e-4cd6-a251-6bd9a112faef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c350ad13-af32-439c-a715-a4c0c523f3f4",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "9d215e3a-b5fa-4af3-97d6-c94475045238"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "621cac79-9b74-46d3-8173-729f434923d0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1feb632c-94fd-4fc4-a2d1-9b9ea667723c",
    "visible": true
}