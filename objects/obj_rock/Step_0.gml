/// @description Move

if (time.paused) {
	image_speed = 0;
	exit;
} else {
	image_speed = anim_speed;
}

y += rock_speed;