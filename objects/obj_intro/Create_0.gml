/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
text = "Long ago, the evil wizard Agharamish stole the Great Tome of Light, bringing darkness to the land, and eternal life to himself.\nHe secluded himself in a castle of his own creation, hiding the book in the depths of its dungeon.\n\nMany centuries have passed, and his castle now shows visible signs of decay - his power must be fading!\nThe Grand Council of Wizards and Magics have appointed you to invade Agharamish's castle and end his reign.\n\n\"In the castle's dungeon you will find two scrolls, each required to open the way to Agharamish. Go now, and good luck!\"\n\n<ENTER> to continue";
next_room = rm_dungeon;