{
    "id": "404ba90f-fe8c-426f-b7c9-653cfd1cf4ad",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_wall",
    "eventList": [
        {
            "id": "a691316b-06c0-4c20-a254-3dfae10a919e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "404ba90f-fe8c-426f-b7c9-653cfd1cf4ad"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "621cac79-9b74-46d3-8173-729f434923d0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2056ddec-0c93-45ef-9dad-48dc611b1483",
    "visible": false
}