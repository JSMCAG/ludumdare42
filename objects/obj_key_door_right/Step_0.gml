/// @description Detect player, open if they have key

if (time.paused) {
	image_speed = 0;
	exit;
} else {
	image_speed = anim_speed;
}

player_near = collision_rectangle(x + 1, y - 16, x + 8, y + 15, obj_player, false, true);

if (player_near) {
	player_near_frame_count++;
} else {
	player_near_frame_count = 0;
}

if (player_near) {
	var has_keys = progression.n_keys > 0;
	if (has_keys) {
		// TODO play a sound
		progression.n_keys--;
		instance_destroy();
	}
}