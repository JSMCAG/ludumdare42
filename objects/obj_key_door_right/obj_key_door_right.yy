{
    "id": "64b046f8-8228-4fa1-b632-bea02b64dd62",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_key_door_right",
    "eventList": [
        {
            "id": "c475f14a-99b3-4f9f-bff6-b5fb5a31407b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "64b046f8-8228-4fa1-b632-bea02b64dd62"
        },
        {
            "id": "90ef0462-22ad-4ae8-bbcb-85d559115f1b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "64b046f8-8228-4fa1-b632-bea02b64dd62"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "7ca6babd-65a9-4b2a-af96-3de4af89f6a9",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "026ccdf9-d46f-4656-8e85-3685259870c5",
    "visible": true
}