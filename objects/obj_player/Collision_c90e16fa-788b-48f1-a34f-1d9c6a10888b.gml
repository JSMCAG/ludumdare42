/// @description Get knockback

if (state != player_states.KNOCKBACK) {
	state = player_states.KNOCKBACK;
	knockback_duration = 30;
	knockback_direction = directions.DOWN;
}

with (other) {
	instance_destroy();
}