{
    "id": "babec998-e324-4134-8cd2-46ed57021d5c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "9f6b8a16-9abb-4522-9b27-bfb344ecd495",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "babec998-e324-4134-8cd2-46ed57021d5c"
        },
        {
            "id": "a3e3e6c3-0787-4c4e-8e3a-7cd3aed4f7d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "babec998-e324-4134-8cd2-46ed57021d5c"
        },
        {
            "id": "c90e16fa-788b-48f1-a34f-1d9c6a10888b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "9d215e3a-b5fa-4af3-97d6-c94475045238",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "babec998-e324-4134-8cd2-46ed57021d5c"
        },
        {
            "id": "96d7af6b-2b32-42ac-b21a-5a5843cbfab0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "55b4e983-525d-48ac-afcd-128ffb85dd5c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "babec998-e324-4134-8cd2-46ed57021d5c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "621cac79-9b74-46d3-8173-729f434923d0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "57f608e2-7f1b-4a07-a7c5-1375b06815a4",
    "visible": true
}