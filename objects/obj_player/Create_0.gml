/// @description Init
event_inherited();

PLAYER_SPEED = 1;

facing = directions.UP;
state = player_states.MOVING;
attack = noone;
knockback_duration = 0;

last_tile_x = x div 8;
last_tile_y = y div 8;