/// @description Move player around and change state

if (time.paused) {
	image_speed = 0;
	exit;
} else {
	image_speed = anim_speed;
}

// INPUT
var up_pressed = keyboard_check(vk_up);
var down_pressed = keyboard_check(vk_down);
var left_pressed = keyboard_check(vk_left);
var right_pressed = keyboard_check(vk_right);

var attack_pressed = keyboard_check(vk_space);

if (state == player_states.MOVING) {
	var new_x = x;
	var new_y = y;

	// MOVEMENT
	if (left_pressed) {
		new_x -= PLAYER_SPEED;
		facing = directions.LEFT;
	} else if (right_pressed) {
		new_x += PLAYER_SPEED;
		facing = directions.RIGHT;
	}

	if (up_pressed) {
		new_y -= PLAYER_SPEED;
		facing = directions.UP;
	} else if (down_pressed) {
		new_y += PLAYER_SPEED;
		facing = directions.DOWN;
	}
	
	switch (facing) {
		case directions.UP:
			sprite_index = spr_player_u;
		    break;
		case directions.DOWN:
			sprite_index = spr_player_d;
		    break;
		case directions.LEFT:
			sprite_index = spr_player_l;
		    break;
		case directions.RIGHT:
			sprite_index = spr_player_r;
		    break;
	}

	if ((new_x != x) || (new_y != y)) {
		scr_check_collisions(new_x, new_y, obj_wall);

		var new_tile_x = x div 8;
		var new_tile_y = y div 8;
		if ((new_tile_x != last_tile_x) || (new_tile_y != last_tile_y)) {
			instance_create_layer(last_tile_x * 8, last_tile_y * 8, "layer_holes", obj_hole);
			last_tile_x = new_tile_x;
			last_tile_y = new_tile_y;
		}
	
		var hole = instance_position(x, y, obj_hole);
		if (hole != noone) {
			with(instance_find(obj_music, 0)) {
				event_user(3);
			}
			state = player_states.FALLING;
			x = hole.x + 4;
			y = hole.y + 6;
		}
	}
	
	// ATTACK
	if (attack_pressed) {
		state = player_states.ATTACKING;
		var attack_spawn_x = x;
		var attack_spawn_y = y;
		var attack_direction = 0;
		switch (facing) {
		    case directions.UP:
				attack_spawn_y -= 8;
		        break;
		    case directions.DOWN:
				attack_spawn_y += 8;
		        break;
		    case directions.LEFT:
		        attack_spawn_x -= 8;
		        break;
		    case directions.RIGHT:
		        attack_spawn_x += 8;
		        break;
		}
		attack = instance_create_layer(attack_spawn_x, attack_spawn_y, layer, obj_attack);
		attack.image_angle = facing;
		attack.facing = facing;
	}
}

if (state == player_states.ATTACKING) {
	
	switch (facing) {
		case directions.UP:
			sprite_index = spr_player_attacking_u;
		    break;
		case directions.DOWN:
			sprite_index = spr_player_attacking_d;
		    break;
		case directions.LEFT:
			sprite_index = spr_player_attacking_l;
		    break;
		case directions.RIGHT:
			sprite_index = spr_player_attacking_r;
		    break;
	}
	
	if (!instance_exists(attack)) {
		attack = noone;
		state = player_states.MOVING;
	}
}

if (state == player_states.FALLING) {
	sprite_index = spr_player_falling;
	if (image_index > image_number - 1) {
		// We have reached the final frame, stop the animation
		image_speed = 0;
		instance_create_layer(x, y, "layer_ui", obj_game_over);
	}
}

if (state == player_states.KNOCKBACK) {
	if (knockback_duration == 0) {
		state = player_states.MOVING;
	} else {
		knockback_duration--;
		
		var new_x = x;
		var new_y = y;
	
		switch (knockback_direction) {
			case directions.UP:
				new_y -= PLAYER_SPEED;
			    break;
			case directions.DOWN:
				new_y += PLAYER_SPEED;
			    break;
			case directions.LEFT:
				new_x -= PLAYER_SPEED;
			    break;
			case directions.RIGHT:
				new_x += PLAYER_SPEED;
			    break;
		}

		if ((new_x != x) || (new_y != y)) {
			scr_check_collisions(new_x, new_y, obj_wall);

			var new_tile_x = x div 8;
			var new_tile_y = y div 8;
			if ((new_tile_x != last_tile_x) || (new_tile_y != last_tile_y)) {
				instance_create_layer(last_tile_x * 8, last_tile_y * 8, "layer_holes", obj_hole);
				last_tile_x = new_tile_x;
				last_tile_y = new_tile_y;
			}
	
			var hole = instance_position(x, y, obj_hole);
			if (hole != noone) {
				with(instance_find(obj_music, 0)) {
					event_user(3);
				}
				state = player_states.FALLING;
				x = hole.x + 4;
				y = hole.y + 6;
			}
		}
	}
	
}