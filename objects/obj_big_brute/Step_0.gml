/// @description Move to same X coordinate as player. Fire at random intervals.

if (time.paused) {
	image_speed = 0;
	exit;
} else {
	image_speed = anim_speed;
}

if (falling) {
	sprite_index = spr_big_brute_falling;
	if (image_index > image_number - 1) {
		// We have reached the final frame, stop the animation
		image_speed = 0;
		instance_destroy();
	}
	exit;
}

next_fire--;

if(next_fire == 0) {
	next_fire = irandom_range(60, 120);
	if (distance_to_object(player) < 64) {
		instance_create_layer(x - 8, y - 12, layer, obj_fire);
		fire_invulnerability = 10;
	}
}

if(knockback > 0) {
	knockback--;
	x += run_speed;
}
	
if (player.y != y) {
	var new_y = y + sign(player.y - y);
	scr_check_collisions(x, new_y, obj_wall);
}

if (fire_invulnerability > 0) {
	fire_invulnerability--;
}
