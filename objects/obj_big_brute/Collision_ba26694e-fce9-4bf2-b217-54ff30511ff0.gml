/// @description Destroy self and rock. Spawn key.

if (fire_invulnerability > 0) {
	exit;
}

with (other) {
	instance_destroy();
}

knockback = 30;
