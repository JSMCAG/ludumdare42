/// @description Init
event_inherited();

run_speed = 1;
player = instance_find(obj_player, 0);

next_fire = 60;
fire_invulnerability = 0;
knockback = 0;
falling = false;