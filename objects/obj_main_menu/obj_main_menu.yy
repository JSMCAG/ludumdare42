{
    "id": "635ec07e-508d-466d-8bea-1f9954f0ca63",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_main_menu",
    "eventList": [
        {
            "id": "0a20e653-3484-4e1f-8c3a-7f0b2b985284",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "635ec07e-508d-466d-8bea-1f9954f0ca63"
        },
        {
            "id": "b7a46a85-e743-463f-9ce9-1a0fe209bbf8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 9,
            "m_owner": "635ec07e-508d-466d-8bea-1f9954f0ca63"
        },
        {
            "id": "bddad7d4-8d36-48c4-8757-24fc3d20b4a7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 38,
            "eventtype": 9,
            "m_owner": "635ec07e-508d-466d-8bea-1f9954f0ca63"
        },
        {
            "id": "25ac8388-9929-405c-b719-b5f911462447",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "635ec07e-508d-466d-8bea-1f9954f0ca63"
        },
        {
            "id": "65a1ad81-73dd-412d-b42d-e22d40d1ecd3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 9,
            "m_owner": "635ec07e-508d-466d-8bea-1f9954f0ca63"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}