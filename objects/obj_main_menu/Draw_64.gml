/// @description Draw menu

display_set_gui_maximize(3, 3);
draw_set_font(fnt_default);
draw_set_halign(fa_left);

draw_rectangle_color(40, 80, 120, 128, c_white, c_white, c_white, c_white, false);

var items_text = "";
for (var i = 0; i < N_ITEMS; ++i) {
	var item_text = items[i];
	if (selected == i) {
		item_text = "> " + item_text;
	}
	items_text += item_text + "\n";
}
draw_text_ext(40, 80, string_upper(items_text), 8, 120);    