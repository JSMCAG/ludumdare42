/// @description Confirm
if (selected == 0) {
	room_goto(rm_intro);
}

if (selected == 1) {
	room_goto(rm_options);
}

if (selected == 2) {
	game_end();
}