{
    "id": "e55ee1d1-bd19-4aeb-bba9-33580a63ae71",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_run_spider",
    "eventList": [
        {
            "id": "a74b900d-9d04-4a0e-bf0c-6a11dc5c999c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c350ad13-af32-439c-a715-a4c0c523f3f4",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e55ee1d1-bd19-4aeb-bba9-33580a63ae71"
        },
        {
            "id": "4888d4a0-d0ed-4fb1-a602-44888b18db49",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e55ee1d1-bd19-4aeb-bba9-33580a63ae71"
        },
        {
            "id": "cd814e3a-683f-4457-b07a-7d7eaf301f65",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e55ee1d1-bd19-4aeb-bba9-33580a63ae71"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "621cac79-9b74-46d3-8173-729f434923d0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "0e61d8e2-c223-49bf-8125-9b1f536f8547",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 256,
            "value": "obj_key",
            "varName": "drop",
            "varType": 5
        }
    ],
    "solid": false,
    "spriteId": "8d76a162-60dc-402d-88ac-606430ac05be",
    "visible": true
}