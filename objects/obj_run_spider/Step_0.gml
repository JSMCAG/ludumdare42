/// @description Try to run away from player

if (time.paused) {
	image_speed = 0;
	exit;
} else {
	image_speed = anim_speed;
}

if (teleport) {
	switch (teleport_phase) {
		case 0:
			sprite_index = spr_run_spider_teleport;
		
			if (image_index > image_number - 1) {
				// We have reached the final frame
				x = teleport_to_x;
				y = teleport_to_y;
				teleport_phase = 1;
			}
			break;
	
		case 1:
			sprite_index = spr_run_spider_return;
		
			if (image_index > image_number - 1) {
				// We have reached the final frame
				teleport = false;
				teleport_phase = 0;
				sprite_index = spr_run_spider;
			}
			break;
	}
		
} else {
	
	var new_x = x;
	var new_y = y;

	if (distance_to_object(player) < radius) {
		new_x += sign(x - player.x) * run_speed;
		new_y += sign(y - player.y) * run_speed;
		
		scr_check_collisions(new_x, new_y, [obj_wall, obj_hole]);
	}
}