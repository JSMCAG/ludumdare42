/// @description Init
event_inherited();

radius = 64;
run_speed = 2;
player = instance_find(obj_player, 0);

teleport = false;
teleport_phase = 0;
teleport_to_x = x;
teleport_to_y = y;