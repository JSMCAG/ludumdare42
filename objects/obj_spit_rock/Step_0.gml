/// @description Move to same X coordinate as player. Fire at random intervals.

if (time.paused) {
	image_speed = 0;
	exit;
} else {
	image_speed = anim_speed;
}

next_spit--;

if(next_spit == 0) {
	next_spit = irandom_range(30, 120);
	instance_create_layer(x, y, layer, obj_rock);
	rock_invulnerability = 10;
}

if (player.x != x) {
	var new_x = x + sign(player.x - x);
	scr_check_collisions(new_x, y, obj_wall);
}

if (rock_invulnerability > 0) {
	rock_invulnerability--;
}