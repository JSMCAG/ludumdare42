/// @description Destroy self and rock. Spawn key.

if (rock_invulnerability > 0) {
	exit;
}

instance_create_layer(drop_x, drop_y, layer, drop);
with (other) {
	instance_destroy();
}
instance_destroy();
