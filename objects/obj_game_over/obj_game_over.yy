{
    "id": "2604c56b-ff23-488a-8bdd-9c30e4402dcf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_game_over",
    "eventList": [
        {
            "id": "aea345d0-d9d8-47f3-89a8-c820f5a14b8e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "2604c56b-ff23-488a-8bdd-9c30e4402dcf"
        },
        {
            "id": "7176df2f-60e9-4b6f-bda7-34b482839c76",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2604c56b-ff23-488a-8bdd-9c30e4402dcf"
        },
        {
            "id": "e87092f9-fbd0-48d7-a08e-23752844aaf5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 9,
            "m_owner": "2604c56b-ff23-488a-8bdd-9c30e4402dcf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "dd55552c-5f17-4750-a26c-8f9fd0a4f769",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "73063a42-06f6-493d-b866-df962de62b85",
            "propertyId": "670b81a3-c705-4042-8dc4-1e9a91d2e2d8",
            "value": "\"GAME OVER\\n\\nDESPITE YOUR GREAT COURAGE, YOU WERE CONSUMED BY THE CRUMBLING DUNGEON.\""
        }
    ],
    "parentObjectId": "73063a42-06f6-493d-b866-df962de62b85",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}