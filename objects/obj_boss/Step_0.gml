/// @description Boss state machine

if (time.paused) {
	image_speed = 0;
	exit;
} else {
	image_speed = anim_speed;
}

switch (state) {
	case boss_states.WAITING:
		if (collision_rectangle(BOSS_ARENA_X1, BOSS_ARENA_Y1, BOSS_ARENA_X2, BOSS_ARENA_Y2, obj_player, false, true)) {
			var txt_box = instance_create_layer(x, y, "layer_ui", obj_text_box);
			txt_box.text = "\"SO WE HAVE FINALLY MET! YOU WISH TO BRING AN END TO MY REIGN? LAUGHABLE!\nBUT ENOUGH TALK!\nHAVE AT YOU!\"";
			with(instance_find(obj_music, 0)) {
				event_user(1);
			}
			state = boss_states.START_MOVING;
		}
		
		break;
	
	case boss_states.START_MOVING:
		with(instance_find(obj_music, 0)) {
			if (current_bgm != mus_boss) {
				event_user(2);
			}
		}
		sprite_index = spr_boss_disappear;
		
		do {
			next_rand_pos_x = irandom_range(BOSS_ARENA_X1, BOSS_ARENA_X2);
			next_rand_pos_y = irandom_range(BOSS_ARENA_Y1, BOSS_ARENA_Y2);
		} until (!place_meeting(next_rand_pos_x, next_rand_pos_y, obj_hole));
		
		if (image_index > image_number - 1) {
			// We have reached the final frame
			instance_create_layer(x, y, "layer_mobs", obj_fake_boss);
			state = boss_states.MOVING;
		}

		break;
		
	case boss_states.MOVING:
		sprite_index = spr_shadow;
		
		move_towards_point(next_rand_pos_x, next_rand_pos_y, 1);
		
		if (abs(point_distance(x, y, next_rand_pos_x, next_rand_pos_y)) < 1) {
			speed = 0;
			state = boss_states.END_MOVING;
		}
		break;
	
	case boss_states.END_MOVING:
		sprite_index = spr_boss_appear;
		
		if (image_index > image_number - 1) {
			// We have reached the final frame
			state = boss_states.START_ATTACKING;
		}
		
		break;
		
	case boss_states.START_ATTACKING:
		sprite_index = spr_boss;
		next_rand_pos_x = irandom_range(BOSS_ARENA_X1, BOSS_ARENA_X2);
		next_rand_pos_y = irandom_range(BOSS_ARENA_Y1, BOSS_ARENA_Y2);
		
		if (image_index > image_number - 1) {
			// We have reached the final frame
			state = boss_states.ATTACKING;
		}
		
		var hole = instance_position(x, y, obj_hole);
		if (hole != noone) {
			state = boss_states.START_FALLING;
			x = hole.x + 4;
			y = hole.y + 6;
		}

		break;
		
	case boss_states.ATTACKING:
		sprite_index = spr_boss_attack;
		instance_create_layer(next_rand_pos_x, next_rand_pos_y, "layer_mobs", obj_lightning);
		state = boss_states.END_ATTACKING;
		
		var hole = instance_position(x, y, obj_hole);
		if (hole != noone) {
			state = boss_states.START_FALLING;
			x = hole.x + 4;
			y = hole.y + 6;
		}

		break;
		
	case boss_states.END_ATTACKING:
		
		if (image_index > image_number - 1) {
			// We have reached the final frame
			state = boss_states.START_MOVING;
		}
		
		var hole = instance_position(x, y, obj_hole);
		if (hole != noone) {
			state = boss_states.START_FALLING;
			x = hole.x + 4;
			y = hole.y + 6;
		}

		break;
		
	case boss_states.START_FALLING:
		var txt_box = instance_create_layer(x, y, "layer_ui", obj_text_box);
		txt_box.text = "\"HOW COULD THIS BE?! FELLED BY MY OWN LAIR?!\nCURSE YOOOUUUUUU!\"";
		state = boss_states.FALLING;
		with(instance_find(obj_music, 0)) {
			event_user(3);
		}
		
		break;
	
	case boss_states.FALLING:
		sprite_index = spr_boss_falling;
		
		if (image_index > image_number - 1) {
			// We have reached the final frame
			room_goto(rm_outro);
		}
}
