/// @description Init state machine

// Inherit the parent event
event_inherited();

BOSS_ARENA_X1 = 824;
BOSS_ARENA_Y1 = 888;
BOSS_ARENA_X2 = 936;
BOSS_ARENA_Y2 = 984;


state = boss_states.WAITING;
next_rand_pos_x = x;
next_rand_pos_y = y
