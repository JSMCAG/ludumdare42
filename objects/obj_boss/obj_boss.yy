{
    "id": "ab0b6493-59c6-4093-8dcc-27cbe8dc479c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_boss",
    "eventList": [
        {
            "id": "2f67428e-3ace-46b5-85ba-4575c44ebcd2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ab0b6493-59c6-4093-8dcc-27cbe8dc479c"
        },
        {
            "id": "1a2f6966-0269-459a-befa-55a712899d48",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ab0b6493-59c6-4093-8dcc-27cbe8dc479c"
        },
        {
            "id": "15f83a39-1fa6-4d46-be1b-28d9e5009f0a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c350ad13-af32-439c-a715-a4c0c523f3f4",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ab0b6493-59c6-4093-8dcc-27cbe8dc479c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "404ba90f-fe8c-426f-b7c9-653cfd1cf4ad",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "09e5189f-ef62-4754-8c7c-20e4c5b1a5da",
    "visible": true
}