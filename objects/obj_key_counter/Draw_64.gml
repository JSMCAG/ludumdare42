/// @description Draw number of keys

display_set_gui_maximize(3, 3);
draw_set_font(fnt_default);
draw_set_halign(fa_left);

draw_rectangle_color(0, 136, 48, 144, c_white, c_white, c_white, c_white, false);
draw_text_ext(0, 136, string_upper("KEYS " + string(progression.n_keys)), 8, 48);