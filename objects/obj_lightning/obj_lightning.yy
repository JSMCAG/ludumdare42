{
    "id": "5f70aabf-3ae6-4ce9-8a1f-6a207f47a624",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_lightning",
    "eventList": [
        {
            "id": "73e6703a-911e-487e-b889-f1f9d44340dc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5f70aabf-3ae6-4ce9-8a1f-6a207f47a624"
        },
        {
            "id": "bd4b8a93-802e-4c10-965f-b6b9f31f3f4f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5f70aabf-3ae6-4ce9-8a1f-6a207f47a624"
        },
        {
            "id": "ac8eb9f3-382a-4d4b-bc92-308c14f8af64",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5f70aabf-3ae6-4ce9-8a1f-6a207f47a624"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "621cac79-9b74-46d3-8173-729f434923d0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1d5e6471-1b9e-4a0d-8a39-46a14ee8bee5",
    "visible": true
}