/// @description Create hole
event_inherited();

linger = 30;

var tile_x = x div 8;
var tile_y = y div 8;

var hole = instance_position(tile_x * 8, tile_y * 8, obj_hole);
if (hole == noone) {
	instance_create_layer(tile_x * 8, tile_y * 8, "layer_holes", obj_hole);
}