/// @description Disapear

if (time.paused) {
	image_speed = 0;
	exit;
} else {
	image_speed = anim_speed;
}

if (!(linger--)) {
	instance_destroy();
}
